# Require
require 'bootstrap-sass'
require 'compass/import-once/activate'

# Paths
http_path = "/"
http_fonts_dir = "public/fonts"
css_dir = "public/stylesheets"
sass_dir = "public/sass"
images_dir = "public/images"
javascripts_dir = "public/javascripts"

# Enable Relative Paths
relative_assets = true