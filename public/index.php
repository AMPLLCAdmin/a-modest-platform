<?php
/**
 * AMODEST PLATFORM
 *
 * The Best Source for the Worst Kind of Info
 * Version: 0.0.2
 * First Stable Update Period: October 2017
 * Last Major Update Period: October 2017
 *
 * Designed by Zach Meyer
 * Core Website for A Modest Platform LLC.
 */
namespace AModestPlatform;

/* -- GLOBALS -- */
define("BASE_PATH",         dirname(__DIR__));
define('APP_PATH',          BASE_PATH . '/app');
define('ENCRYPTION_KEY',    "!@895jfl|}{*^gh56!@xkpz89bvcA#&!");
define('CSRF_KEY',          "_cj.z`,*SRYmsRmp0s+f>Re|iI@8pMZ^l}]+DRe1Rb_Al3iTZg'K(Xby)7et(:2|4");

/* -- CUSTOM COMPONENTS -- */
// Helpers
require_once(APP_PATH . "/plugins/helpers/ArticleParsingHelper.php");
require_once(APP_PATH . "/plugins/helpers/PageLayoutHelper.php");
require_once(APP_PATH . "/plugins/helpers/CSRFProtectorHelper.php");
require_once(APP_PATH . "/plugins/helpers/UserInfoHelper.php");

// Managers
require_once(APP_PATH . "/plugins/managers/PostsManager.php");
require_once(APP_PATH . "/plugins/managers/UserManager.php");

/* -- EXTERNAL DEPENDENCIES -- */
// Phalcon - Base
use Phalcon\Loader;
use Phalcon\Crypt;
use Phalcon\Security;

// Phalcon MVC
use Phalcon\Mvc\View;
use Phalcon\Mvc\Dispatcher;
use Phalcon\Mvc\Application;
use Phalcon\Mvc\View\Engine\Volt;
use Phalcon\Mvc\Url as UrlProvider;

// Phalcon DependencyInjector (Factory Default)
use Phalcon\Di\FactoryDefault;

// Phalcon Cookies & Session
use Phalcon\Session\Adapter\Files as SessionAdapter;
use Phalcon\Http\Response\Cookies as CookieMonster;

// Phalcon DB
use Phalcon\Db\Adapter\Pdo\Mysql as DbAdapter;

// Phalcon Config
use Phalcon\Config\Adapter\Ini as ConfigIni;

/* -- INTERNAL DEPENDENCIES -- */
// Helpers
use AModestPlatform\Plugins\Helpers\CSRFProtectorHelper;
use AModestPlatform\Plugins\Helpers\ArticleParsingHelper;
use AModestPlatform\Plugins\Helpers\PageLayoutHelper;
use AModestPlatform\Plugins\Helpers\EventsHelper;
use AModestPlatform\Plugins\Helpers\UserInfoHelper;

// Managers
use AModestPlatform\Plugins\Managers\PostsManager;
use AModestPlatform\Plugins\Managers\UserManager;

/* -- CONFIGURE -- */
/**
 * @var $config _configPrototype
 */
$config = new ConfigIni(APP_PATH . "/config/config.ini");

/* -- LOADER -- */
// Create Loader
$loader = new Loader();

// Register Namespaces
$loader->registerNamespaces(
    [
        // MVC
        "AModestPlatform\Controllers"       => APP_PATH . $config->directories->controllersDir,
        "AModestPlatform\Models"            => APP_PATH . $config->directories->modelsDir,
        "AModestPlatform\Views"             => APP_PATH . $config->directories->viewsDir,
        // Plugins
        "AModestPlatform\Plugins\Helpers"   => APP_PATH . $config->directories->pluginsHelpersDir,
        "AModestPlatform\Plugins\Managers"  => APP_PATH . $config->directories->pluginsManagersDir,
        "AModestPlatform\Plugins"           => APP_PATH . $config->directories->pluginsRootDir,
        // Other
        "AModestPlatform\Forms"             => APP_PATH . $config->directories->formsDir
    ]
);
$loader->register();

/* -- DEPENDENCY INJECTION -- */
// Create full-stack DI
$di = new FactoryDefault();

// Register Config as a Service
$di->set(
    'config',
    function() use ($config) {
        return $config;
    }
);

// Register Events Helper (operating as an encapsulated Event Manager) as a Shared Service
$di->setShared(
    'eventsManager',
    function() use ($di) {
        $eventsHelper = new EventsHelper($di);
        return $eventsHelper;
    }
);

// Register Dispatcher as a Service
$di->set(
    'dispatcher',
    function() use ($config, $di) {
        $eHelper = $di->getShared('eventsManager');

        // Initialize event handling helpers
        $eHelper->initErrorHandling();
        $eHelper->initParameterHandling();

        $dispatcher = new Dispatcher();
        $dispatcher->setDefaultNamespace($config->application->defaultControllerNamespace);
        $dispatcher->setEventsManager(EventsHelper::$EventsManager);

        return $dispatcher;
    }
);

// Register Volt as a Service
$di->set(
    'voltService',
    function ($view, $di) use ($config) {
        $volt = new Volt($view, $di);

        // Set Volt options
        $volt->setOptions(
            [
                'compileAlways' => $config->application->alwaysCompileVolt,
                'compiledPath' => APP_PATH . $config->directories->voltCompiledDir,
                'compiledSeparator' => '_',
                'compiledExtension' => '.voltc'
            ]
        );

        return $volt;
    }
);

// Register View as a Service
$di->set(
    'view',
    function() use ($config) {
        $view = new View();
        $view->registerEngines(
            [
                '.volt' => 'voltService'
            ]
        );
        $view->setViewsDir(APP_PATH . $config->directories->viewsDir);

        return $view;
    }
);

// Register Database as a Service
$di->set(
    'db',
    function() use ($config) {
        return new DbAdapter(
            [
                'host'      => $config->database->host,
                'username'  => $config->database->username,
                'password'  => $config->database->password,
                'dbname'    => $config->database->dbname
            ]
        );
    }
);

// Register Session as a Shared Service
$di->setShared(
    'session',
    function()
    {
        $session = new SessionAdapter();
        $session->start();

        return $session;
    }
);

// Register Base URI
$di->set(
    'url',
    function () use ($config) {
        $url = new UrlProvider();
        $url->setBaseUri($config->directories->serverURI);
        return $url;
    }
);

// Register Router
$di->set(
    'router',
    function () {
        $router = require(APP_PATH . '/config/routes.php');
        return $router;
    }
);

// Register Crypt
$di->set(
    'crypt',
    function() use ($config) {
        $crypt = new Crypt();

        $crypt->setKey(ENCRYPTION_KEY);

        return $crypt;
    },
    true
);

// Register Cookies
$di->set(
    'cookies',
    function() {
        $cookies = new CookieMonster();
        $cookies->useEncryption(false);
        return $cookies;
    }
);

// Register Security
$di->set(
    'security',
    function() {
        $security = new Security();

        $security->setWorkFactor(12);

        return $security;
    }
);

/* -- REGISTER HELPERS -- */
// ArticleParsing Helper
$di->set(
    'articleParser',
    function() use ($di) {
        return new ArticleParsingHelper($di);
    }
);

// PageLayout Helper
$di->set(
    'pageLayout',
    function () use ($di) {
        return new PageLayoutHelper($di);
    }
);

// CSRFProtector Helper
$di->set(
    'csrfProtector',
    function() use ($di) {
        return new CSRFProtectorHelper($di);
    }
);

// UserInfo Helper
$di->set(
    'userInfo',
    function() use ($di) {
        return new UserInfoHelper($di);
    }
);

/* -- REGISTER MANAGERS -- */
// Posts Manager
$di->set(
    'postsManager',
    function() use ($di) {
        return new PostsManager($di);
    }
);

// User Manager
$di->set(
    'userManager',
    function() use ($di) {
        return new UserManager($di);
    }
);

/* -- INSTANTIATE APPLICATION -- */
// Create Application
$application = new Application($di);

// Process Requests
try
{
    $response = $application->handle();
    $response->send();
}
catch (\Exception $e)
{
    echo "A Modest Platform's application encountered a fatal error. Apologies for the inconvenience, please try again later.";
    echo "<br /><br />";
    echo "Message: " . $e->getMessage();
    echo "<br /><br />";
    echo "Stack Trace: " . $e->getTraceAsString();
}





