$(document).ready(function() {
    /* -- DECLARE INTERFACES -- */
    // Quill Interface
    var quillInterface = new QuillInterface();

    // Treeview Interface
    var treeviewInterface = new TreeviewInterface();
    treeviewInterface.quillInterfaceInstance = quillInterface;

    // Image Archive Builder Interface
    var imgArchiveBuilderInterface = new ImgArchiveBuilderInterface();

    /* -- DOM ELEMENTS -- */
    var btnSubmitChanges = $("#submitChanges");
    var btnCancelEdits = $("#cancelEdits");
    var btnToggleArticleImageList = $("#toggle-article-image-list");
    var btnBrowseForFiles = $("#browse-for-image-button");
    var btnCreateArticle = $("#article-create-button");
    var inputFiles = $("#upload-image");
    var divUploadLoading = $("#upload-image-loading");

    /* -- DECLARATIONS -- */
    var shouldSave = false;
    var isArticleFromLoad = false;
    var selectedParentNode = false;
    var articleImageListCollapsed = false;

    /* -- EVENT OVERRIDES -- */
    // Treeview Interface - Node Selected
    treeviewInterface.event_onNodeSelected = function(event, data) {
        if (data.tags != undefined) {
            var articleRefId = data.tags[0];
            var quill = this.quillInterfaceInstance;

            if (articleRefId != "") {
                quill.flash("Loading...");
                axios.get(REDIRECT_PATH_HEADER + 'axios/articlecontent/refId:' + articleRefId, {
                    headers: {
                        'X-Requested-With': 'XMLHttpRequest'
                    }
                }).then(function (response) {
                    quill.setQuillContent(response.data, true);
                    quill.currentlyLoadedArticleRefId = articleRefId;
                    isArticleFromLoad = true;

                    $("#article-settings").show();
                    imgArchiveBuilderInterface.fetchImageList("#article-image-list");
                });
            }
        }
        else {
            this.unselectTreeNode("#articleTree", data.nodeId, true);
        }
    }

    // Treeview Interface - Node Unselected
    treeviewInterface.event_onNodeUnselected = function(event, data) {
        var quill = this.quillInterfaceInstance;
        if (shouldSave) {
            var leave = confirm("Are you sure you want to stop editing this article? Unsaved changes will be lost!");
            if (leave == true) {
                unloadArticle();
            }
            else {
                this.selectTreeNode("#articleTree", data.nodeId, true);
            }
        }
        else {
            unloadArticle();
        }
    }

    // Quill Interface - Content Changed
    quillInterface.event_onContentChanged = function(delta, source) {
        if (quillInterface.currentContent != quillInterface.loadedContent) {
            flipSaveButton();
            shouldSave = true;
        }

        quillInterface.currentContent = quillInterface.getQuillContent();
    }

    // Quill Interface - Insert Image
    quillInterface.event_onInsertImage = function() {
        var imageIndexSelected = null;

        // Create the Dialog
        $("#modal-image-selector").dialog({
            modal: true,
            autoOpen: false,
            show: "blind",
            hide: "blind",
            width: 550,
            appendTo: "#contribute",
            buttons: {
                Ok: function() {
                    var altText = $("#imageAltText").val();

                    if (altText != "") {
                        quillInterface.insertImage(imageIndexSelected, altText);
                    }
                    else {
                        quillInterface.insertImage(imageIndexSelected, "[Article Image]");
                    }

                    imageIndexSelected = null;
                    $("#modal-image-selector-inner").empty();
                    $("#imageAltText").prop("disabled", true);
                    $(this).dialog("close");
                },
                Cancel: function() {
                    imageIndexSelected = null;
                    $("#modal-image-selector-inner").empty();
                    $(this).dialog("close");
                }
            }
        });

        axios.get(REDIRECT_PATH_HEADER + 'axios/imgarchivelist/refId:' + quillInterface.currentlyLoadedArticleRefId, {
            headers: {
                'X-Requested-With': 'XMLHttpRequest'
            }
        }).then(function(response) {
            // Show Dialog
            $("#modal-image-selector-inner").html(response.data);
            $("#modal-image-selector").dialog("open");

            // Register Click Event Bindings
            $("#modal-image-selector-inner").on("click", "li", function() {
                imageIndexSelected = $(this).attr("id");

                $("#modal-image-selector-inner ul li").removeClass("ia-list-item-selected");
                $(this).addClass("ia-list-item-selected");

                $("#imageAltText").prop("disabled", false);
            });
        });
    }

    // Button - Save Content
    btnSubmitChanges.on("click", function() {
        var doSave = confirm("Are you sure you wish to save your changes? This cannot be undone.");
        var saveSuccessful = false;

        if (doSave == true) {
            axios.post(REDIRECT_PATH_HEADER + 'axios/savearticle/refId:' + quillInterface.currentlyLoadedArticleRefId,
            {
                articleContent: quillInterface.getQuillContent()
            },
            {
                headers: {
                    'X-Requested-With': 'XMLHttpRequest'
                }
            }).then(function(response) {
                alertSaveSuccessful();
                flipSaveButton(false);
            }).catch(function(error) {
                alertSaveUnsuccessful();
            });
        }
    });

    // Button - Cancel Edits
    btnCancelEdits.on("click", function() {
        var revert = confirm("Are you sure you want to revert? All unsaved changes will be lost!");
        if (revert == true) {
            axios.get(REDIRECT_PATH_HEADER + 'axios/articlecontent/refId:' + quillInterface.currentlyLoadedArticleRefId, {
                headers: {
                    'X-Requested-With': 'XMLHttpRequest'
                }
            }).then(function(response) {
                quillInterface.setQuillContent(response.data, true);

                flipSaveButton(false);

                shouldSave = false;
            });
        }
    });

    // Button - Collapse Image Archive List
    btnToggleArticleImageList.on("click", function() {
        if (!articleImageListCollapsed) {
            articleImageListCollapsed = true;
            btnToggleArticleImageList.text("Expand");
            $("#article-image-list").slideUp("slow");
        }
        else {
            articleImageListCollapsed = false;
            btnToggleArticleImageList.text("Collapse");
            $("#article-image-list").slideDown("slow");
        }

    });

    // Button - Upload File Click
    btnBrowseForFiles.on("click", function() {
        inputFiles.click();
    });

    // Button - Create New Article
    btnCreateArticle.on("click", function() {
        // Create the Dialog
        $("#modal-create-new-article").dialog({
            modal: true,
            autoOpen: false,
            show: "blind",
            hide: "blind",
            width: 550,
            appendTo: "#contribute",
            buttons: {
                Ok: function() {

                },
                Cancel: function() {
                    $(this).dialog("close");
                }
            }
        });

        //Show the Dialog
        $("#modal-create-new-article").dialog("open");
    });

    // Archive Settings - Fetch Image List
    imgArchiveBuilderInterface.event_onFetchImageList = function(divImageList) {
        this.currentImageListDiv = divImageList;

        axios.get(REDIRECT_PATH_HEADER + 'axios/imgarchivelist/refId:' + quillInterface.currentlyLoadedArticleRefId, {
            headers: {
                'X-Requested-With': 'XMLHttpRequest'
            }
        }).then(function(response) {
            $(divImageList).html(response.data);
        });
    }

    // Archive Settings - Upload Image
    imgArchiveBuilderInterface.event_onUploadImage = function(uploadFormData) {
        axios.post(IMAGE_SERVER_PATH + "image.php?refid=" + quillInterface.currentlyLoadedArticleRefId + "&type=upload", uploadFormData)
            .then(function (response) {
                alert(response.data);

                divUploadLoading.hide();

                imgArchiveBuilderInterface.fetchImageList("#article-image-list");
            });
    }

    /* -- JQUERY EVENT BINDINGS -- */
    // QuillEditor DOMSubtree Modified - Allows detection of innerHTML styling changes to trigger event_onContentChanged
    $("#quillEditor").on("DOMSubtreeModified", function(event) {
        if (quillInterface != null) {
            quillInterface.event_onContentChanged(null, event);
        }
    });

    // Upload File for Image Archive
    inputFiles.on("change", function() {
        divUploadLoading.show();

        var uploadFormData = new FormData();
        var file = this.files[0];

        uploadFormData.append('fileData', file);

        imgArchiveBuilderInterface.upload(uploadFormData);
    });

    /* -- INIT QUILL AND TREEVIEW -- */
    // Build Toolbar
    quillInterface.initToolbar("#quillToolbar");

    // Initialize QuillJS
    quillInterface.initQuill("#quillEditor");

    // Build the tree data
    treeviewInterface.initTreeData("#articleTreeData");

    // Initialize Treeview
    treeviewInterface.initTreeview("#articleTree");

    /* -- INIT INDIVIDUAL ARTICLE COMPONENTS -- */
    // Initialize Article Settings Accordion
    $("#article-settings-accordion").accordion({
        active: false,
        collapsible: true,
        heightStyle: "content"
    });

    /* -- MISCELLANEOUS HELPER FUNCTIONS -- */
    // Flip Save Button
    function flipSaveButton(isEnabled = true) {
        if (isEnabled) {
            btnSubmitChanges.prop("disabled", false);
            btnSubmitChanges.removeClass("disabled-button");

            btnCancelEdits.prop("disabled", false);
            btnCancelEdits.removeClass("disabled-button");
        }
        else {
            btnSubmitChanges.prop("disabled", true);
            btnSubmitChanges.addClass("disabled-button");

            btnCancelEdits.prop("disabled", true);
            btnCancelEdits.addClass("disabled-button");
        }
    }

    // Unload Article
    function unloadArticle() {
        shouldSave = false;
        isArticleFromLoad = false;
        isFreshlyLoaded = false;

        quillInterface.clearQuill();
        imgArchiveBuilderInterface.reset();
        flipSaveButton(false);
        $("#article-settings").hide();

    }

    /* -- SHOW UI -- */
    flipSaveButton(false);
    $("#loadingIndicator").hide();
    $("#contribute").show();
});