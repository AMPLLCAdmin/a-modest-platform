/* -- CLASS QuillInterface -- */
function QuillInterface() {}

/* -- FIELDS -- */
// Quill Instance
QuillInterface.prototype.quillInstance = null;

// Toolbar Buttons
QuillInterface.prototype.toolbarButtons = [
    'ql-bold',
    'ql-italic',
    'ql-underline',
    'ql-strike',
    new Array('ql-header', 1, 2, 3, 4, 5, 6),
    new Array('ql-align', 'right', 'center', 'justify'),
    'ql-link',
    'ql-image',
    'ql-clean'
];

// Track Changes
QuillInterface.prototype.currentlyLoadedArticleRefId = "";
QuillInterface.prototype.loadedContent = "";
QuillInterface.prototype.currentContent = "";

/* -- PUBLIC METHODS -- */
// Initialize Quill
QuillInterface.prototype.initQuill = function(divEditor) {
    // Import fonts
    var quillFont = Quill.import('formats/font');
    quillFont.whitelist = ['Archivo Black', 'Noto Serif'];
    Quill.register(quillFont, true);

    // Import inline styles
    var quillAlign = Quill.import('attributors/style/align');
    Quill.register(quillAlign, true);

    // Register Blots
    Quill.register(EmbeddedImage);

    // Get Instance of Quill Interface
    var inst = this;

    // Init QuillJS
    this.quillInstance = new Quill(divEditor, {
        theme: 'snow',
        modules: {
            toolbar: {
                container: '#quillToolbar',
                handlers: {
                    image: inst.event_onInsertImage
                }
            },
            clipboard: {
                matchVisual: false
            },
            imageResize: {}
        }
    });

    // Set Text-Change Event
    this.quillInstance.on('text-change', function(delta, source) {
        inst.event_onContentChanged(delta, source);
    });
}

// Initialize Toolbar
QuillInterface.prototype.initToolbar = function(divToolbar) {
    var innerHTML = "";
    for(var button = 0, len = this.toolbarButtons.length; button < len; button++)
    {
        if (Array.isArray(this.toolbarButtons[button]) == false) {
            innerHTML += "<button class='" + this.toolbarButtons[button] + "'></button>";
        }
        else {
            innerHTML += "<select class='" + this.toolbarButtons[button][0] + "'>";
            for (var option = 1; option < this.toolbarButtons[button].length; option++) {
                innerHTML += "<option value='" + this.toolbarButtons[button][option] + "'></option>";
            }
            innerHTML += "</select>";
        }
    }
    $(divToolbar).html(innerHTML);
}

// Add Custom Toolbar Button
QuillInterface.prototype.addCustomToolbarButton = function(divToolbar, buttonId, buttonText = "Custom") {
    var buttonHTML = "<button id='" + buttonId + "'>" + buttonText + "</button>";
    $(divToolbar).append(buttonHTML);
}

// Set Quill Content
QuillInterface.prototype.setQuillContent = function(content, isRaw = false) {
    if (isRaw) {
        this.quillInstance.root.innerHTML = content;
        this.loadedContent = content;
        this.currentContent = content;
    }
    else {
        this.quillInstance.setText(content);
        this.loadedContent = content;
        this.currentContent = content;
    }
}

// Flash
QuillInterface.prototype.flash = function(message) {
    this.quillInstance.setText(message);
}

// Clear Quill Cache
QuillInterface.prototype.clearQuill = function() {
    this.loadedContent = "";
    this.currentContent = "";
    this.currentlyLoadedArticleRefId = "";
    this.quillInstance.setText("");
}

// Get Quill Content
QuillInterface.prototype.getQuillContent = function() {
    if (this.quillInstance != null) {
        return this.quillInstance.root.innerHTML;
    }
}

// Save Content
QuillInterface.prototype.saveContent = function() {
    return this.event_onSaveContent();
}

// Insert Image
QuillInterface.prototype.insertImage = function(imageIndex, altText) {
    var range = this.quillInstance.getSelection(true);
    this.quillInstance.insertEmbed(range.index + 1, 'image', {
        alt: altText,
        url: IMAGE_SERVER_PATH + "image.php?refid=" + this.currentlyLoadedArticleRefId + "&type=archive&index=" + imageIndex
    }, Quill.sources.USER);
    this.quillInstance.setSelection(range.index + 2, Quill.sources.SILENT);
}

/* -- EVENT DECLARATIONS -- */
// On Content Changed
QuillInterface.prototype.event_onContentChanged = function(delta, source) {}

// Save Content
QuillInterface.prototype.event_onSaveContent = function() {}

// On Insert Image
QuillInterface.prototype.event_onInsertImage = function() {}

/* -- CUSTOM BLOTS -- */
var BlockEmbed = Quill.import("blots/block/embed");

// Image Blot
class EmbeddedImage extends BlockEmbed
{
    static create(value) {
        let node = super.create();
        node.setAttribute('alt', value.alt);
        node.setAttribute('src', value.url);
        node.setAttribute('class', 'img-fluid');
        return node;
    }

    static value(node) {
        return {
            alt: node.getAttribute('alt'),
            url: node.getAttribute('src')
        };
    }
}
EmbeddedImage.blotName = "image";
EmbeddedImage.tagName = "img";

