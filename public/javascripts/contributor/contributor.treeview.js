/* -- CLASS TreeviewInterface -- */
function TreeviewInterface() {}

/* -- FIELDS -- */
// Treeview Interface
TreeviewInterface.prototype.activeTreeview = null;

// Tree Data
TreeviewInterface.prototype.treeData = [];

// Quill Interface Instance
TreeviewInterface.prototype.quillInterfaceInstance = null;

/* -- PUBLIC METHODS -- */
// Initialize the Treeview
TreeviewInterface.prototype.initTreeview = function(divTreeview) {
    var inst = this;
    $(divTreeview).treeview(
        {
            data: this.treeData,
            levels: 1,

            // On Node Selected Event
            onNodeSelected: function(event, data) {
                inst.event_onNodeSelected(event, data);
            },

            // On Node Unselected
            onNodeUnselected: function(event, data) {
                inst.event_onNodeUnselected(event, data);
            }
        }
    );
}

// Select Tree Node
TreeviewInterface.prototype.selectTreeNode = function(divTreeview, nodeId, silently = false) {
    var inst = this;
    $(divTreeview).treeview(true).selectNode(nodeId, { silent: silently })
}

// Unselect Tree Node
TreeviewInterface.prototype.unselectTreeNode = function(divTreeview, nodeId, silently = false) {
    var inst = this;
    $(divTreeview).treeview(true).unselectNode(nodeId, { silent: silently })
}

// Init Tree Data
TreeviewInterface.prototype.initTreeData = function(treeDataDiv) {
    // Hide the data div
    $(treeDataDiv).hide();

    var pushToTreeData = [];

    // Get the node tags
    $(treeDataDiv).children('x\\:node').each(function() {
        var articleCategory = $(this).attr("category");
        var articles = [];

        // Get all the articles for that category
        $(this).children('x\\:subnode').each(function() {
            var articleTitle = $(this).attr("title");
            var articleRefId = $(this).text();
            articles.push({
                text: articleTitle,
                icon: "glyphicon glyphicon-pencil",
                tags: [articleRefId]
            });
        });

        // Create a tree object
        var treeObject = {
            text: articleCategory,
            nodes: []
        };

        // Populate the tree object
        for (var article = 0, len = articles.length; article < len; article++)
        {
            treeObject.nodes.push(articles[article]);
        }

        pushToTreeData.push(treeObject);
    });

    this.treeData = pushToTreeData;
}

/* -- EVENT DECLARATIONS -- */
// On Node Selected
TreeviewInterface.prototype.event_onNodeSelected = function(event, data) {}

// On Node Unselected
TreeviewInterface.prototype.event_onNodeUnselected = function(event, data) {}