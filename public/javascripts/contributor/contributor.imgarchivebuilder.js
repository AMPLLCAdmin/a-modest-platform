/* -- CLASS ImgArchiveBuilderInterface -- */
var ImgArchiveBuilderInterface = function() {}

/* -- FIELDS -- */
ImgArchiveBuilderInterface.prototype.currentImageListDiv = null;

/* -- PUBLIC METHODS -- */
// Fetch Image List
ImgArchiveBuilderInterface.prototype.fetchImageList = function(divImageList)
{
    currentImageListDiv = divImageList;
    this.event_onFetchImageList(divImageList);
}

// Reset
ImgArchiveBuilderInterface.prototype.reset = function() {
    $(this.currentImageListDiv).empty();
}

// Upload Image
ImgArchiveBuilderInterface.prototype.upload = function(uploadFormData) {
    this.event_onUploadImage(uploadFormData);
}

/* -- EVENT HANDLERS -- */
ImgArchiveBuilderInterface.prototype.event_onFetchImageList = function(divImageList) {}
ImgArchiveBuilderInterface.prototype.event_onUploadImage = function(uploadFormData) {}
