// SITE ALERTS //
// Powered by iziToast: http://izitoast.marcelodolce.com/ //

// New Registration Success Alert
function alertNewRegistration()
{
    iziToast.success({
        id: 'regSuccess',
        class: 'ampaccount-messageext-pushdown',
        title: 'Registration Successful',
        message: 'Thanks for registering, you may now login! Please make sure to check your e-mail and validate it!',
        position: 'topRight',
        transitionIn: 'bounceInLeft',
        timeout: 3000
    });
}

// Logged Out Alert
function alertLoggedOut()
{
    iziToast.success({
        id: 'regSuccess',
        class: 'ampaccount-messageext-pushdown',
        title: 'Successfully Logged Out',
        message: 'You have been succesfully logged out! See you again soon!',
        position: 'topRight',
        transitionIn: 'bounceInLeft',
        timeout: 3000
    });
}

// Registration Failed Alert
function alertRegistrationFailed(errorMsg)
{
    iziToast.error({
        id: 'regFormInvalid',
        class: 'ampaccount-messageext-pushdown',
        title: 'Whoops, Something Went Wrong',
        message: errorMsg,
        position: 'topRight',
        transitionIn: 'bounceInLeft',
        timeout: 3000
    });
}

// CSRF Check Failed Alert
function alertCSRFCheckFailed()
{
    iziToast.error({
        id: 'csrfCheckFailed',
        class: 'ampaccount-messageext-pushdown',
        title: 'CSRF Check Failed',
        message: 'Your request to login was blocked due to suspicious activity. Restart your browser and try again.',
        position: 'topRight',
        transitionIn: 'bounceInLeft',
        timeout: 3000
    });
}

// CSRF Check Failed Alert
function alertFailedLogin()
{
    iziToast.error({
        id: 'csrfCheckFailed',
        class: 'ampaccount-messageext-pushdown',
        title: 'Login Failed',
        message: 'Validation failed! Please verify your login details and try again.',
        position: 'topRight',
        transitionIn: 'bounceInLeft',
        timeout: 3000
    });
}

// Article Save Unsuccessful Alert
function alertSaveUnsuccessful()
{
    iziToast.error({
        id: 'contributeSaveUnsuccessful',
        class: 'ampaccount-messageext-pushdown',
        title: 'Save Unsuccessful',
        message: 'An error occurred while saving your changes to this article. We apologize for the inconvenience.',
        position: 'topRight',
        transitionIn: 'bounceInLeft',
        timeout: 3000
    });
}

// Article Save Successful Alert
function alertSaveSuccessful()
{
    iziToast.success({
        id: 'regSuccess',
        class: 'ampaccount-messageext-pushdown',
        title: 'Save Successful',
        message: 'Changes to your article have been saved successfully!',
        position: 'topRight',
        transitionIn: 'bounceInLeft',
        timeout: 3000
    });
}