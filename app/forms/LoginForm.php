<?php
namespace AModestPlatform\Forms;

/* -- DEPENDENCIES -- */
// Phalcon
use Phalcon\Di;
use Phalcon\Forms\Form;
use Phalcon\Forms\Element\Text;
use Phalcon\Forms\Element\Password;
use Phalcon\Forms\Element\Submit;
use Phalcon\Forms\Element\Check;
use Phalcon\Validation\Validator\PresenceOf;

/* -- CLASS LoginForm -- */
class LoginForm extends Form
{
    /* -- PUBLIC CLASS METHODS -- */
    // Initialize
    public function initialize()
    {
        // emailOrUsername Field
        $id = new Text('id', [
            'placeholder' => 'Email or Username'
        ]);
        $id->addValidator(new PresenceOf([
            'message' => 'Please enter your username or e-mail'
        ]));
        $this->add($id);

        // password Field
        $password = new Password('password', [
            'placeholder' => 'Password'
        ]);
        $password->addValidator(new PresenceOf([
            'message' => 'Please enter a password'
        ]));
        $this->add($password);

        // rememberMe Field
        $rememberMe = new Check('rememberMe', [
            'value' => 'yes'
        ]);
        $this->add($rememberMe);

        // Submit
        $this->add(new Submit('submit'));
    }
}
