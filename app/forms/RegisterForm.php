<?php
namespace AModestPlatform\Forms;

/* -- DEPENDENCIES -- */
// Phalcon
use Phalcon\Di;
use Phalcon\Forms\Form;
use Phalcon\Forms\Element\Text;
use Phalcon\Forms\Element\Password;
use Phalcon\Forms\Element\Submit;
use Phalcon\Forms\Element\Check;
use Phalcon\Validation\Validator\PresenceOf;
use Phalcon\Validation\Validator\StringLength;
use Phalcon\Validation\Validator\Email;
use Phalcon\Validation\Validator\Confirmation;
use Phalcon\Validation\Validator\Uniqueness;

// Internal
use AModestPlatform\Models\User;

/* -- CLASS RegisterForm -- */
class RegisterForm extends Form
{
    /* -- PUBLIC CLASS METHODS -- */
    // Initialize
    public function initialize()
    {
        // username Field
        $username = new Text('username', [
            'placeholder'   => 'Username'
        ]);
        $username->addValidator(new PresenceOf([
            'message'       => 'Please enter your username'
        ]));
        $username->addValidator(new StringLength([
            'max'            => 30,
            'min'            => 6,
            'messageMaximum' => 'Username cannot be more than 30 characters long.',
            'messageMinimum' => 'Please enter a username that is at least 6 characters'
        ]));
        $username->addValidator(new Uniqueness([
            'model'     => new User(),
            'field'     => 'username',
            'message'   => 'Sorry, this username already exists. Please try again.'
        ]));
        $this->add($username);

        // email Field
        $email = new Text('email', [
            'placeholder'   => 'E-Mail (example: you@gmail.com)'
        ]);
        $email->addValidator(new PresenceOf([
            'message'   => 'Please enter your e-mail address'
        ]));
        $email->addValidator(new Email([
            'message'   => 'The e-mail you have entered is invalid'
        ]));
        $email->addValidator(new Uniqueness([
            'model'     => new User(),
            'field'     => 'email',
            'message'   => 'There is already an account associated with this e-mail.'
        ]));
        $this->add($email);

        // password Field
        $password = new Password('password', [
            'placeholder'   => 'Password'
        ]);
        $password->addValidator(new PresenceOf([
            'message'   => 'Please enter a password'
        ]));
        $password->addValidator(new StringLength([
            'max'            => 75,
            'min'            => 6,
            'messageMaximum' => 'Try again with a password you\'re more likely to remember (<75 characters)',
            'messageMinimum' => 'Please enter a password that is at least 6 characters'
        ]));
        $password->addValidator(new Confirmation([
            'message'   => 'Passwords do not match',
            'with'      => 'confirmPassword'
        ]));
        $this->add($password);

        // confirmPassword Field
        $confirmPassword = new Password('confirmPassword', [
            'placeholder' => 'Password again'
        ]);
        $confirmPassword->addValidator(new PresenceOf([
            'message' => 'Please confirm your password'
        ]));
        $this->add($confirmPassword);

        // checkContributor Field
        $checkContributor = new Check('checkContributor', [
            'value' => 'yes'
        ]);
        $this->add($checkContributor);

        /* -- FOR CONTRIBUTORS ONLY -- */
        // fullName Field
        $fullName = new Text('fullName', [
            'placeholder' => 'Real First and Last Name'
        ]);
        $this->add($fullName);

        // Submit
        $this->add(new Submit('submit'));
    }
}