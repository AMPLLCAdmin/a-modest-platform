<?php
namespace AModestPlatform\Models;

/* -- DEPENDENCIES -- */
use Phalcon\Mvc\Model;

/* -- CLASS Post Model -- */
class Post extends Model
{
    /* -- PRIVATE DECLARATIONS -- */
    protected $id;
    protected $article_ref_id;
    protected $contributor_id;
    protected $posted_date;
    protected $last_edit_date;
    protected $title;
    protected $category;
    protected $published;
    protected $highlight;

    /* -- GET -- */
    // Article Ref Id
    public function getArticleRefId()
    {
        return $this->article_ref_id;
    }

    // Contributor Id
    public function getContributorId()
    {
        return $this->contributor_id;
    }

    // Posted Date
    public function getPostedDate()
    {
        //Format our posted date
        $rawtime = $this->posted_date;
        return date('m/d/Y', strtotime($rawtime));
    }

    // Last Edit Date
    public function getLastEditDate()
    {
        return $this->last_edit_date;
    }

    // Title
    public function getTitle()
    {
        return $this->title;
    }

    // URL Friendly Title
    public function getURLFriendlyTitle()
    {
        $title = $this->title;
        $urlFriendlyTitle = strtolower(str_replace(' ', '_', $title));
        return $urlFriendlyTitle;
    }

    // Category
    public function getCategory()
    {
        return $this->category;
    }

    // Get Published
    public function getPublished($asString)
    {
        $published = $this->published;
        if ($asString === true)
        {
            if ($published == 0)
            {
                return "Unpublished";
            }
            else
            {
                return "Published";
            }
        }
        else
        {
            return $published;
        }
    }

    // Get Highlight
	public function getHighlight()
	{
		return $this->highlight;
	}

    /* -- CLASS METHODS -- */
    // Initialize
    public function initialize()
    {
        $this->setSource('amp_posts');
    }
}