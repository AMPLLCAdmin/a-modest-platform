<?php
namespace AModestPlatform\Models;

/* -- DEPENDENCIES -- */
use Phalcon\Mvc\Model;

/* -- CLASS User Model -- */
class UserCognizance extends Model
{
    /* -- PROTECTED -- */
    protected $id;
    protected $token;
    protected $expiry;

    /* -- GET -- */
    // Token
    public function getToken()
    {
        return $this->token;
    }

    // Expiry
    public function getExpiry()
    {
        return $this->expiry;
    }

    /* -- SET -- */
    // Token
    public function setToken($token)
    {
        $this->token = $token;
    }

    // Expiry
    public function setExpiry($expiry)
    {
        $this->expiry = $expiry;
    }

    /* -- CLASS METHODS -- */
    // Initialize
    public function initialize()
    {
        $this->setSource('amp_user_cognizance');
    }
}