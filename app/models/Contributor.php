<?php
namespace AModestPlatform\Models;

/* -- DEPENDENCIES -- */
use Phalcon\Mvc\Model;

/* -- CLASS Post Model -- */
class Contributor extends Model
{
    /* -- PRIVATE DECLARATIONS -- */
    protected $id;
    protected $name;
    protected $approved;
    protected $bio;
    protected $pic;

    /* -- GET -- */
    // ID
    public function getID()
    {
        return $this->id;
    }

    // Name
    public function getName()
    {
        return $this->name;
    }

    // Approved
    public function getApproved()
    {
        return $this->approved;
    }

    // Bio
    public function getBio()
    {
        return $this->bio;
    }

    // Pic
    public function getPic()
    {
        return $this->pic;
    }

    /* -- SET -- */
    // Set Name
    public function setName($name)
    {
        $this->name = $name;
    }

    // Set Approved
    public function setApproved($approved)
    {
        $this->approved = $approved;
    }

    // Set Bio
    public function setBio($bio)
    {
        $this->bio = $bio;
    }

    // Set Pic
    public function setPic($picData)
    {
        $this->pic = $picData;
    }

    /* -- CLASS METHODS -- */
    // Initialize
    public function initialize()
    {
        $this->setSource('amp_contributors');
    }
}