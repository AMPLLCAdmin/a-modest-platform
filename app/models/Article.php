<?php
namespace AModestPlatform\Models;

/* -- DEPENDENCIES -- */
use Phalcon\Mvc\Model;

/* -- CLASS Post Model -- */
class Article extends Model
{
    /* -- PRIVATE DECLARATIONS -- */
    protected $id;
    protected $ref_id;
    protected $content;

    /* -- GET -- */
    // Get Ref ID
    public function getRefId()
    {
        return $this->ref_id;
    }

    // Get Content
    public function getContent()
    {
        return $this->content;
    }

    /* -- SET -- */
    public function setRefId($refId)
    {
        $this->ref_id = $refId;
    }

    // Set Content
    public function setContent($content)
    {
        $this->content = $content;
    }

    /* -- CLASS METHODS -- */
    // Initialize
    public function initialize()
    {
        $this->setSource('amp_articles');
    }
}
