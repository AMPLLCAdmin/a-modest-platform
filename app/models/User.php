<?php
namespace AModestPlatform\Models;

/* -- DEPENDENCIES -- */
use Phalcon\Mvc\Model;

/* -- CLASS User Model -- */
class User extends Model
{
    /* -- PROTECTED -- */
    protected $id;
    protected $username;
    protected $password;
    protected $email;
    protected $contributor_id;
    protected $permission_level;
    protected $session_id;
    protected $last_login;

    /* -- GET -- */
    // Username
    public function getUsername()
    {
        return $this->username;
    }

    // Password
    public function getPassword()
    {
        return $this->password;
    }

    // Email
    public function getEmail()
    {
        return $this->email;
    }

    // Contributor ID
    public function getContributorId()
    {
        if ($this->contributor_id == 0)
        {
            return false;
        }
        else
        {
            return $this->contributor_id;
        }
    }

    // Permission Level
    public function getPermissionLevel()
    {
        return $this->permission_level;
    }

    // Session ID
    public function getSessionId()
    {
        return $this->session_id;
    }

    // Last Login
    public function getLastLogin()
    {
        return date("F j, Y g:i a", strtotime($this->last_login));
    }

    /* -- SET -- */
    // Username
    public function setUsername($username)
    {
        $this->username = $username;
    }

    // Password
    public function setPassword($password)
    {
        $this->password = $password;
    }

    // E-mail
    public function setEmail($email)
    {
        $this->email = $email;
    }

    // Contributor ID
    public function setContributorId($id)
    {
        $this->contributor_id = $id;
    }

    // Permission Level
    public function setPermissionLevel($level)
    {
        $this->permission_level = $level;
    }

    // Session ID
    public function setSessionId($id)
    {
        $this->session_id = $id;
    }

    // Set Last Login
    public function setLastLogin($last_login)
    {
        $this->last_login = $last_login;
    }

    /* -- CLASS METHODS -- */
    // Initialize
    public function initialize()
    {
        $this->setSource('amp_users');
    }
}