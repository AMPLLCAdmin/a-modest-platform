<?php
namespace AModestPlatform\Models;

/* -- DEPENDENCIES -- */
use Phalcon\Mvc\Model;

/* -- CLASS Post Model -- */
class Category extends Model
{
    /* -- PRIVATE DECLARATIONS -- */
    protected $category_name;

    /* -- GET -- */
    // Name
    public function getCategoryName()
    {
        return $this->category_name;
    }

    /* -- CLASS METHODS -- */
    // Initialize
    public function initialize()
    {
        $this->setSource('amp_categories');
    }
}