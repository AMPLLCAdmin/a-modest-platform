<?php
# Routes Config #

/* -- DEPENDENCIES -- */
use Phalcon\Mvc\Router;

/* -- ROUTER OBJECT -- */
$router = new Router();

// MAPS /articles/
$router->add(
    '/articles/:params',
    [
        'controller'    => 'articles',
        'action'        => 'index',
        'params'        => 1
    ]
);

// MAPS /axios/
$router->add(
    '/axios/:action/:params',
    [
        'controller'    => 'axios',
        'action'        => 1,
        'params'        => 2
    ]
);

// Maps /account/[action]/
$router->add(
    '/account/:action/:params',
    [
        'controller'    => 'account',
        'action'        => 1,
        'params'        => 2
    ]
);

//MAPS /account/test-login/[type]
$router->add(
    '^/account/test-login/(.*?)$',
    [
        'controller'    => 'account',
        'action'        => 'testlogin',
        'type'          => 1
    ]
);

/* -- RETURN ROUTER -- */
$router->handle();
return $router;