<?php
namespace AModestPlatform\Controllers;

/* -- DEPENDENCIES -- */
// Phalcon
use Phalcon\Mvc\Controller;

// PHPDoc
use AModestPlatform\_configPrototype;
use AModestPlatform\Plugins\Helpers\UserInfoHelper;
use AModestPlatform\Plugins\Helpers\ArticleParsingHelper;
use AModestPlatform\Plugins\Helpers\PageLayoutHelper;
use AModestPlatform\Plugins\Managers\PostsManager;

/* -- CLASS IndexController -- */
/**
 * @property _configPrototype $config
 * @property UserInfoHelper $userInfo
 * @property PostsManager $postsManager
 * @property ArticleParsingHelper $articleParser
 * @property PageLayoutHelper $pageLayout
 *
 */
class IndexController extends Controller
{
    /* -- ACTIONS -- */
    // INDEX - Default Page
    public function indexAction()
    {
        // Config
        $pathHeader = $this->config->url->redirectPathHeader;

        // Controller is aware
        $this->userInfo->initAwareness();

        // Set our managers to their respective volt counterparts
        $this->view->setVar('posts', $this->postsManager);
        $this->view->setVar('parser', $this->articleParser);

        // Set volt variables
        $this->view->setVar('pathHeader', $pathHeader);

        // Load our CSS and JS files from the LayoutHelper plugin
        $this->pageLayout->loadGlobalStylesAndJS($this);
    }
}