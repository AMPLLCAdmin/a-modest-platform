<?php
namespace AModestPlatform\Controllers;

/* -- DEPENDENCIES -- */
// Phalcon
use Phalcon\Mvc\Controller;

// PHPDoc
use AModestPlatform\_configPrototype;

// Internal
use AModestPlatform\Plugins\Managers\AxiosManager;

/* -- CLASS AxiosController -- */
/**
 * @property _configPrototype $config
 */
class AxiosController extends Controller
{
    /* -- PROTECTED -- */
    /** @var $axios AxiosManager */
    protected $axios;

    /* -- CONSTRUCTOR -- */
    public function initialize()
    {
        $this->axios = new AxiosManager($this->di);
    }

    /* -- ACTIONS -- */
    // Index
    public function indexAction()
    {
        // Forbid access to this controller
        $this->response->setStatusCode(403);
    }

    /* --- CONTRIBUTOR PANEL ACTIONS --- */
    // GET - Article Content
    public function articlecontentAction()
    {
        if ($this->checkRequestValidity())
        {
            if ($this->request->isGet())
            {
                $this->view->disable();

                $refId = $this->dispatcher->getParam('refId');
                echo $this->axios->getArticleContent($refId);
            }
            else
            {
                $this->response->setStatusCode(400);
            }
        }
        else
        {
            $this->response->setStatusCode(403);
        }
    }

    // POST - (Save) Article Content
    public function savearticleAction()
    {
        if ($this->checkRequestValidity())
        {
            if ($this->request->isPost())
            {
                $this->view->disable();

                $refId = $this->dispatcher->getParam('refId');

                $data = $this->request->getJsonRawBody(true);
                $postContent = $data['articleContent'];

                echo $this->axios->updateArticleContent($refId, $postContent);
            }
            else
            {
                $this->response->setStatusCode(400);
            }
        }
        else
        {
            $this->response->setStatusCode(403);
        }
    }

    //GET - IMAGE ARCHIVE LIST
    public function imgarchivelistAction()
    {
        if ($this->checkRequestValidity())
        {
            if ($this->request->isGet())
            {
                $this->view->disable();

                $refId = $this->dispatcher->getParam('refId');

                echo $this->axios->getImageArchiveList($refId);
            }
            else
            {
                $this->response->setStatusCode(400);
            }
        }
        else
        {
            $this->response->setStatusCode(403);
        }
    }


    /* -- PROTECTED FUNCTIONS -- */
    protected function checkRequestValidity()
    {
        if ($this->request->isAjax())
        {
            return true;
        }

        return false;
    }
}