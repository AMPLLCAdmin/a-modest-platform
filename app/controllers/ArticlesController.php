<?php
namespace AModestPlatform\Controllers;

/* -- DEPENDENCIES -- */
// Phalcon
use Phalcon\Mvc\Controller;

// PHPDoc
use AModestPlatform\_configPrototype;
use AModestPlatform\Plugins\Helpers\UserInfoHelper;
use AModestPlatform\Plugins\Helpers\ArticleParsingHelper;
use AModestPlatform\Plugins\Helpers\PageLayoutHelper;
use AModestPlatform\Plugins\Managers\PostsManager;

/* -- CLASS ArticlesController -- */
/**
 * @property _configPrototype $config
 * @property UserInfoHelper $userInfo
 * @property PostsManager $postsManager
 * @property ArticleParsingHelper $articleParser
 * @property PageLayoutHelper $pageLayout
 *
 */
class ArticlesController extends Controller
{
    /* -- ACTIONS -- */
    // Index
    public function indexAction()
    {
        // Config
        $pathHeader = $this->config->url->redirectPathHeader;
        $cssDir = $this->config->directories->baseURI . $this->config->directories->stylesDir;

        // Set our managers and helpers to their respective volt counterparts
        $this->view->setVar('posts', $this->postsManager);
        $this->view->setVar('parser', $this->articleParser);

        // Set volt configurations and variables
        $this->view->setVar('pathHeader', $pathHeader);
        $this->view->setVar('ref_id', $this->dispatcher->getParam('refId')); // RefId

        // Controller is aware
        $this->userInfo->initAwareness();

        $easyTitle = $this->dispatcher->getParam('title');
        $modTitle = ucwords(str_replace('_', ' ', $easyTitle));
        $this->view->setVar('easy_title', $modTitle);

        // Load our CSS and JS files from the LayoutHelper plugin
        $this->pageLayout->loadGlobalStylesAndJS($this);
        $this->pageLayout->loadSpecificInternal($this, $cssDir . "parser.css");
    }
}