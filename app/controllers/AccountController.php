<?php
/*
 * Remember To Do
 *
 * --* Cookies still do not work, "Remember Me" functionality is currently disabled (COOKIES_WORK constant)
 *
 * --* Test Login Action is a debug function that really should be removed in production...maybe
 * -----but in the mean time the function is left in and protected. One caveat is that it is NOT future-proof!
 */
namespace AModestPlatform\Controllers;

/* -- DEPENDENCIES -- */
// Phalcon
use Phalcon\Mvc\Controller;

// PHPDoc
use AModestPlatform\_configPrototype;
use AModestPlatform\Plugins\Managers\UserManager;
use AModestPlatform\Plugins\Helpers\PageLayoutHelper;
use AModestPlatform\Plugins\Helpers\CSRFProtectorHelper;

// Internal
use AModestPlatform\Forms\LoginForm;
use AModestPlatform\Forms\RegisterForm;

/* -- CLASS AccountController -- */
/**
 * @property _configPrototype $config
 * @property UserManager $userManager
 * @property PageLayoutHelper $pageLayout
 * @property CSRFProtectorHelper $csrfProtector
 */
class AccountController extends Controller
{
    /* -- ACTIONS -- */
    // Index
    public function indexAction()
    {
        $this->view->disable();     // Don't bother rendering a view
        $redirectPathHeader = $this->config->url->redirectPathHeader;

        // Check if the user is logged in before displaying this page
        if (!$this->userManager->cognizant())
        {
            $this->response->redirect($redirectPathHeader . "account/login");
        }
        else
        {
            $this->response->redirect($redirectPathHeader . "account/main");
        }
    }

    // Login
    public function loginAction()
    {
        // Config
        $redirectPathHeader = $this->config->url->redirectPathHeader;
        $jsDir = $this->config->directories->baseURI . $this->config->directories->jsDir;
        $libDir = $this->config->directories->baseURI . $this->config->directories->libDir;

        // Load our page layout
        $this->pageLayout->loadGlobalStylesAndJS($this);
        $this->pageLayout->loadSpecificInternal($this, $libDir . "iziToast-master/dist/css/iziToast.min.css");

        // Load our JS
        $this->pageLayout->loadSpecificInternal($this, $libDir . "iziToast-master/dist/js/iziToast.min.js", "JS");
        $this->pageLayout->loadSpecificInternal($this, $jsDir . "siteAlerts.js", "JS");

        // Initialize login form
        $loginForm = new LoginForm();
        $this->view->form = $loginForm;

        // Set View Vars
        $this->view->setVar("pathHeader", $redirectPathHeader);
        $this->view->setVar("csrf", $this->csrfProtector->CSRF());

        /* -- FORWARDED FROM REGISTRATION OR LOGOUT */
        $forwardAction = $this->dispatcher->getParam('action');
        if ($forwardAction != "")
        {
            $this->view->setVar("forwarded", "yes");
            $this->view->setVar("forwardedAction", $forwardAction);
        }

        // Logging In
        if ($this->request->isPost())
        {
            $validateCSRF = $this->csrfProtector->validateCSRF($this->request->getPost('token'));
            if ($validateCSRF)
            {
                try
                {
                    $result = $this->userManager->login($this->request->getPost());
                    if ($result[0] == true)
                    {
                        $this->response->redirect($redirectPathHeader . "account/");
                    }
                    else
                    {
                        $this->response->redirect($redirectPathHeader . "account/login/action:failed");
                    }
                }
                catch (\Exception $e)
                {
                    $this->response->redirect($redirectPathHeader . "account/login/action:failed");
                }
            }
            else
            {
                $this->view->setVar("csrfFailed", "yes");
            }
        }
    }

    // Register
    public function registerAction()
    {
        // Config
        $redirectPathHeader = $this->config->url->redirectPathHeader;
        $jsDir = $this->config->directories->baseURI . $this->config->directories->jsDir;
        $libDir = $this->config->directories->baseURI . $this->config->directories->libDir;

        // Load our page layout
        $this->pageLayout->loadGlobalStylesAndJS($this);
        $this->pageLayout->loadSpecificInternal($this, $libDir . "iziToast-master/dist/css/iziToast.min.css");

        // Load our JS
        $this->pageLayout->loadSpecificInternal($this, $libDir . "iziToast-master/dist/js/iziToast.min.js", "JS");
        $this->pageLayout->loadSpecificInternal($this, $jsDir . "siteAlerts.js", "JS");

        // Set view vars
        $this->view->setVar("pathHeader", $redirectPathHeader);

        /* -- FORWARDED FROM ANOTHER ACTION -- */
        $forwardAction = $this->dispatcher->getParam('action');
        if ($forwardAction != "")
        {
            $this->view->setVar("forwarded", "yes");
            $this->view->setVar("forwardedAction", $forwardAction);
        }

        // Initialize registration form
        $registerForm = new RegisterForm();
        $this->view->form = $registerForm;
        $this->view->setVar("csrf", $this->csrfProtector->CSRF());

        // Logging In
        if ($this->request->isPost())
        {
            if ($registerForm->isValid($this->request->getPost()) != false)
            {
                $validateCSRF = $this->csrfProtector->validateCSRF($this->request->getPost('token'));
                if ($validateCSRF)
                {
                    $registerAction = $this->userManager->register($this->request);
                    if ($registerAction !== false)
                    {
                        $this->response->redirect($redirectPathHeader . "account/login/action:registerSuccess");
                    }
                    else
                    {
                        $this->response->redirect($redirectPathHeader . "/account/register/action:registrationFailed");
                    }
                }
                else
                {
                    $this->view->setVars([
                        'formWasInvalid' => 'yes',
                        'invalidMsg' => 'Cross-Site Request Forgery Detected! Registration not submitted!'
                    ]);
                }
            }
            else
            {
                $invalidMsg = $registerForm->getMessages();
                $this->view->setVars([
                    'formWasInvalid' => 'yes',
                    'invalidMsg' => $invalidMsg[0]
                ]);
            }
        }
    }

    // Log Out
    public function logoutAction()
    {
        $redirectPathHeader = $this->config->url->redirectPathHeader;

        if ($this->userManager->cognizant()["Result"] == true)
        {
            if ($this->userManager->logout()["Result"] == true)
            {
                $this->response->redirect($redirectPathHeader . "account/login/action:loggedOut");
            }
        }
        else
        {
            $this->response->redirect($redirectPathHeader . "/account");
        }
    }
}