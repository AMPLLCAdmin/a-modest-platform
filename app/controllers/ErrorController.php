<?php
namespace AModestPlatform\Controllers;

error_reporting(E_ALL);
/* -- DEPENDENCIES -- */
// PHPDoc
use AModestPlatform\_configPrototype;
use AModestPlatform\Plugins\Helpers\PageLayoutHelper;
use AModestPlatform\Plugins\Helpers\UserInfoHelper;

// Phalcon
use Phalcon\Mvc\Controller;

/* -- CLASS ArticlesController -- */
/**
 * @property _configPrototype $config
 * @property PageLayoutHelper $pageLayout
 * @property UserInfoHelper $userInfo
 */
class ErrorController extends Controller
{
    /* -- ERROR CODES (Analogous to Actions) -- */
    // 404
    public function handle404Action()
    {
        // Config
        $pathHeader = $this->config->url->redirectPathHeader;

        // Set volt variables
        $this->view->setVar('pathHeader', $pathHeader);

        // Controller is aware
        $this->userInfo->initAwareness();

        // Display the error page to the user
        $this->response->setStatusCode(404, 'Not Found');
        $this->pageLayout->loadGlobalStylesAndJS($this);
        $this->view->pick('error/404');
    }
}
