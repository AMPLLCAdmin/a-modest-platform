<?php
namespace AModestPlatform\Controllers;

/* -- DEPENDENCIES -- */
// Phalcon
use Phalcon\Mvc\Controller;

// PHPDoc
use AModestPlatform\_configPrototype;
use AModestPlatform\Plugins\Helpers\UserInfoHelper;
use AModestPlatform\Plugins\Helpers\PageLayoutHelper;
use AModestPlatform\Plugins\Helpers\ArticleParsingHelper;
use AModestPlatform\Plugins\Managers\UserManager;
use AModestPlatform\Plugins\Managers\PostsManager;

/* -- CLASS ContributeController -- */
/**
 * @property _configPrototype $config
 * @property UserInfoHelper $userInfo
 * @property PageLayoutHelper $pageLayout
 * @property UserManager $userManager
 * @property PostsManager $postsManager
 * @property ArticleParsingHelper $articleParser
 */
class ContributeController extends Controller
{
    /* -- ACTIONS -- */
    // Index
    public function indexAction()
    {
        $this->view->disable();     // Don't bother rendering a view
        $redirectPathHeader = $this->config->url->redirectPathHeader;

        // Check if the user is logged in before displaying this page
        if (!$this->userManager->cognizant())
        {
            $this->response->redirect($redirectPathHeader . "account/login");
        }
        else
        {
            $this->response->redirect($redirectPathHeader . "contribute/desk");
        }
    }

    // Desk - Default Page
    public function deskAction()
    {
        // Config
        $pathHeader = $this->config->url->redirectPathHeader;
        $jsDir = $this->config->directories->baseURI . $this->config->directories->jsDir;
        $libDir = $this->config->directories->baseURI . $this->config->directories->libDir;

        // Controller is aware
        $user = $this->userInfo->initAwareness();

        // Set our managers to their respective volt counterparts
        $this->view->setVar('posts', $this->postsManager);
        $this->view->setVar('parser', $this->articleParser);

        // Set volt variables
        $this->view->setVar('pathHeader', $pathHeader);

        // Load our CSS and JS files from the LayoutHelper plugin
        $this->pageLayout->loadGlobalStylesAndJS($this);

        // -- Load external files
        $this->pageLayout->loadSpecificExternal($this, "//cdn.quilljs.com/1.3.4/quill.snow.css", "CSS", false);
        $this->pageLayout->loadSpecificExternal($this, "//cdn.quilljs.com/1.3.4/quill.min.js", "JS", false);
        $this->pageLayout->loadSpecificExternal($this, "//unpkg.com/axios/dist/axios.min.js", "JS", false);

        // -- Load internal files, use the $this->load override to save typing (lots of files)
        $this->load($jsDir . "config.js");
        $this->load($jsDir . "siteAlerts.js");

        $this->load($libDir . "iziToast-master/dist/css/iziToast.min.css");
        $this->load($libDir . "iziToast-master/dist/js/iziToast.min.js");

        $this->load($libDir . "bootstrap-treeview/bootstrap-treeview.min.css");
        $this->load($libDir . "bootstrap-treeview/bootstrap-treeview.min.js");

        $this->load($libDir . "quill-plugins/image-resize.min.js");

        $this->load($jsDir . "contributor/contributor.core.js");
        $this->load($jsDir . "contributor/contributor.quill.js");
        $this->load($jsDir . "contributor/contributor.treeview.js");
        $this->load($jsDir . "contributor/contributor.imgarchivebuilder.js");
    }

    /* -- PROTECTED FUNCTIONS -- */
    // $this->pageLayout->loadSpecificInternal override
    protected function load($fileName)
    {
        if (strrpos($fileName, ".js", -2) !== false)
        {
            $this->pageLayout->loadSpecificInternal($this, $fileName, "JS");
            return true;
        }
        else if (strrpos($fileName, ".css", -3) !== false)
        {
            $this->pageLayout->loadSpecificInternal($this, $fileName, "CSS");
            return true;
        }
        else
        {
            return false;
        }
    }
}