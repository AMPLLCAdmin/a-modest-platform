<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>A Modest Platform - The Best Source for the Worst Kind of Info</title>
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="description" content="A Modest Platform is anything but modest, more amodest - the opposite of modest.
                                                        Seeking to bring you compelling opinions, facts, humor, and fun activities from the turbulent world
                                                        of current events.">
        {# -- OUTPUT CSS -- #}
        {{ assets.outputCss() }}
    </head>
    <body id="page-body">
    {# -- PAGE HEADER -- #}
    {% include 'header.volt' %}
    <div id="account" class="page">
        <div class="account-container">
            {{ form('class' : 'form-horizontal') }}
            <fieldset>
                <legend>Please Log In</legend>
                <div class="form-group">
                    <label for="id" class="col-lg-2 control-label">ID</label>
                    <div class="col-lg-10">
                        {{ form.render('id', ['class' : 'form-control']) }}
                    </div>
                </div>
                <div class="form-group">
                    <label for="password" class="col-lg-2 control-label">Password</label>
                    <div class="col-lg-10">
                        {{ form.render('password', ['class' : 'form-control']) }}
                        <div class="checkbox">
                            <label>
                                {{ form.render('rememberMe') }}Remember Me (for 7 days)
                            </label>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-lg-10 col-lg-offset-2">
                        <input type="hidden" id="token" name="token" value="{{ csrf }}" />
                        <button id="btnSubmit" type="submit" class="btn btn-primary">Submit</button>
                        <button id="btnRegister" type="button" class="btn btn-default" onclick="registerClick()">Register</button>
                    </div>
                </div>
            </fieldset>
            </form>
        </div>
    </div>
    {# -- PAGE FOOTER -- #}
    {% include 'footer.volt' %}
    {# -- OUTPUT JS -- #}
    {{ assets.outputJs() }}
    <script type="text/javascript">
        function registerClick()
        {
            window.location = '{{ pathHeader }}account/register';
        }

        {# Alert for a successful new registration #}
        $(document).ready(function() {
            {% if forwarded is defined %}
            {% if forwardedAction == "registerSuccess" %}
            // Alert for successful new registration
            alertNewRegistration();
            {% elseif forwardedAction == "loggedOut"  %}
            // Alert for successfully logged out
            alertLoggedOut();
            {% elseif forwardedAction == "failed" %}
            // Alert for failed login
            alertFailedLogin();
            {% else %}
            {# DO NOTHING #}
            {% endif %}
            {% endif %}

            {# Alert for a failed CSRF check #}
            {% if csrfFailed is defined %}
            // Alert for failed CSRF check
            alertCSRFCheckFailed();
            {% endif %}
        });
    </script>
    </body>
</html>