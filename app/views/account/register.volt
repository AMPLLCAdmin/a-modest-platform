<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>A Modest Platform - The Best Source for the Worst Kind of Info</title>
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="description" content="A Modest Platform is anything but modest, more amodest - the opposite of modest.
                                                        Seeking to bring you compelling opinions, facts, humor, and fun activities from the turbulent world
                                                        of current events.">
        {# -- OUTPUT CSS -- #}
        {{ assets.outputCss() }}
    </head>
    <body id="page-body">
    {# -- PAGE HEADER -- #}
    {% include 'header.volt' %}
    <div id="account" class="page">
        <div class="account-container">
            {{ form('class' : 'form-horizontal') }}
            <fieldset>
                <legend>Enter Your Registration Details</legend>
                <div class="form-group">
                    <label for="username" class="col-lg-2 control-label">Username</label>
                    <div class="col-lg-10">
                        {{ form.render('username', ['class' : 'form-control']) }}
                    </div>
                </div>
                <div class="form-group">
                    <label for="email" class="col-lg-2 control-label">E-mail</label>
                    <div class="col-lg-10">
                        {{ form.render('email', ['class' : 'form-control']) }}
                    </div>
                </div>
                <div class="form-group">
                    <label for="password" class="col-lg-2 control-label">Password</label>
                    <div class="col-lg-10">
                        {{ form.render('password', ['class' : 'form-control']) }}
                    </div>
                </div>
                <div class="form-group">
                    <label for="confirmPassword" class="col-lg-2 control-label">Confirm Password</label>
                    <div class="col-lg-10">
                        {{ form.render('confirmPassword', ['class' : 'form-control']) }}
                        <div class="checkbox">
                            <label>
                                {{ form.render('checkContributor') }}I Would Like To Be a Contributor
                            </label>
                        </div>
                    </div>
                </div>
                <div id="fullNameContainer" class="form-group" style="display: none;">
                    <label for="fullName" class="col-lg-2 control-label">Your Full Name</label>
                    <div class="col-lg-10">
                        {{ form.render('fullName', ['class' : 'form-control']) }}
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-lg-10 col-lg-offset-2">
                        <input type="hidden" id="token" name="token" value="{{ csrf }}" />
                        <button id="btnSubmit" type="submit" class="btn btn-primary">Submit</button>
                        <button id="btnReset" type="reset" class="btn btn-default">Reset</button>
                    </div>
                </div>
            </fieldset>
            </form>
        </div>
    </div>
    {# -- PAGE FOOTER -- #}
    {% include 'footer.volt' %}
    {# -- OUTPUT JS -- #}
    {{ assets.outputJs() }}
    <script type="text/javascript">
        // Check if the contributor checkbox has changed
        $(document).ready(function() {

            // Enable Contributor Form Fields
            $('#checkContributor').change(
                function() {
                    if ($(this).is(':checked')) {
                        $("#fullNameContainer").css("display", "block");
                    }
                    else {
                        $("#fullNameContainer").css("display", "none");
                    }
                }
            );

            {# Alert for invalid registration details #}
            {% if formWasInvalid is defined %}
            // Alert for invalid registration details
            alertRegistrationFailed('{{ invalidMsg }}');
            {# Alert for failed registration #}
            {% endif %}

            {# Alert for failed registration task #}
            {% if forwarded is defined %}
            {% if forwardedAction == "registrationFailed" %}
            alertRegistrationFailed('Registration task failed due to unforseen problem. Your account was not created. Please try again later.');
            {% endif %}
            {% endif %}
        });
    </script>
    </body>
</html>