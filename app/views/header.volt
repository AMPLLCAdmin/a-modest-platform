<div class="container logo-container root-element">
    <img src="{{ pathHeader }}public/images/placeholder-logo.jpg" onClick="window.location='{{ pathHeader }}'" />
</div>
<div class="container root-element" align="center">
    <nav class="navbar navbar-default" data-spy="affix" data-offset-top="147">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#collapsible-navbar">
                <span class="sr-only">Toggle Navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand hidden-md hidden-lg" href="{{ pathHeader }}">Navigation</a>
        </div>
        <div class="collapse navbar-collapse" id="collapsible-navbar">
            <ul class="nav navbar-nav">
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">Opinions <span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li><a href="{{ pathHeader }}opinions/">Spotlight</a></li>
                        <li class="divider"></li>
                        <li><a href="{{ pathHeader }}opinions/politics">Politics</a></li>
                        <li><a href="{{ pathHeader }}opinions/scitech">Science &amp; Technology</a></li>
                        <li><a href="{{ pathHeader }}opinions/healthmed">Health &amp; Medicine</a></li>
                        <li><a href="{{ pathHeader }}opinions/cryptocurrency">Cryptocurrency</a></li>
                    </ul>
                </li>
                <li class="nav-item"><a href="{{ pathHeader }}blurbs" class="nav-link">Blurbs</a></li>
                <li class="nav-item"><a href="{{ pathHeader }}factchecker" class="nav-link">FactChecker</a></li>
                <li class="nav-item"><a href="{{ pathHeader }}humor" class="nav-link">Humor</a></li>
                <li class="nav-item"><a href="{{ pathHeader }}stories" class="nav-link">Stories</a></li>
                <li class="nav-item"><a href="{{ pathHeader }}interactive" class="nav-link">Interactive</a></li>
            </ul>
            <ul class="nav navbar-nav navbar-right">
                {% if loggedIn is defined %}
                    <li class="dropdown">
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#"><span class="glyphicon glyphicon-user"></span> My Account <span class="caret"></span></a>
                        <ul class="dropdown-menu">
                            <li><a href="{{ pathHeader }}account"><span class="glyphicon glyphicon-home"></span> Home</a></li>
                            <li><a href="{{ pathHeader }}account/settings"><span class="glyphicon glyphicon-cog"></span> Settings</a></li>
                            <li><a href="{{ pathHeader }}account/logout"><span class="glyphicon glyphicon-log-out"></span> Log Out</a></li>
                        </ul>
                    </li>
                {% else %}
                    <li><a href="{{ pathHeader }}account"><span class="glyphicon glyphicon-user"></span> Login / My Account</a></li>
                {% endif %}
                {% if isContributor is defined %}
                    {% if isContributor == 0 %}
                    <li><a href="{{ pathHeader }}account/contributor/apply"><span class="glyphicon glyphicon-send"></span> Become a Contributor</a></li>
                    {% else %}
                    <li><a href="{{ pathHeader }}contribute"><span class="glyphicon glyphicon-send"></span> Contribute</a></li>
                    {% endif %}
                {% endif %}
                <li class="nav-item"><a href="#" class="nav-link"><span class="glyphicon glyphicon-search"></span>&nbsp;&nbsp;Search</a></li>
            </ul>
        </div>
    </nav>
</div>