<div class="footer">
    <div class="inner container">
        <h5>This website is intended for mature audiences only due to language and topics discussed.</h5>
        All content herein is the intellectual property of A Modest Platform LLC. and its contributing staff. Copyright Pending.
        <br />
        <br />
        <a href="{{ pathHeader }}about">About Us</a> |
        <a href="{{ pathHeader }}legal/privacy">Privacy Policy</a> |
        <a href="{{ pathHeader }}legal/terms">Terms of Use</a> |
        <a href="{{ pathHeader }}legal/cookies">Cookies Agreement</a> |
        <a href="{{ pathHeader }}legal/licenses">Software Licenses</a>
        <br />
        <br />
    </div>
</div>
<!-- CREDITS -->
<!--
    iziToast provided by http://www.marcelodolce.com/
    bootstrap-treeview provided by Johnathan Miles at https://github.com/jonmiles
    quill-image-resize-module provided by Ken Snyder at https://github.com/kensnyder
    axios provided by https://github.com/axios

    All rights reserved to these individuals for the use of their software on this website. Please support them!
-->