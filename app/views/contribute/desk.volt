<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>A Modest Platform - The Best Source for the Worst Kind of Info</title>
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="description" content="A Modest Platform is anything but modest, more amodest - the opposite of modest.
                                                            Seeking to bring you compelling opinions, facts, humor, and fun activities from the turbulent world
                                                            of current events.">
        {# -- OUTPUT CSS -- #}
        {{ assets.outputCss() }}
        <!-- QUILL JS STYLE OVERRIDES -->
        <style type="text/css">
            .ql-container { font-family: "Noto Serif", sans-serif !important; }
            .ql-header { font-family: "Archivo Black", sans-serif !important; }
            .ql-header .ql-picker-label svg { padding: 0 0 0 6px !important; }
        </style>
        <!-- SASS-NOT-NECESSARY -->
        <style type="text/css">
            .align-buttons-right { text-align: right; }
            .settings-divider { margin: 7px 0 15px 0; }
            #article-settings { display: none; }
            #modal-image-selector { display: none; }
            #modal-create-new-article { display: none; }
            .article-create { width: 100%; text-align: center; margin-bottom: 5px; }
            .smaller-text { font-size: 11px; }
            #upload-image { display: none; }
            #upload-image-loading { display: none; font-size: 13px !important; }
            .article-title-textbox { width: 75%; }
            .loading-img { width: 15px; height: 15px; float: left; }

        </style>
        <!-- JQUERY UI OVERRIDES -->
        <style type="text/css">
            .ui-widget-content a { color: #44b3ea !important; }
        </style>
    </head>
    <body id="page-body">
        {# -- PAGE HEADER -- #}
        {% include 'header.volt' %}
        <div class="page">
            <!-- LOADING INDICATOR -->
            <div id="loadingIndicator">Loading Desk, Please Wait...</div>
            <!-- PAGE CONTENT -->
            <div id="contribute">
                <div class="grid-row">
                    <!-- YOUR ARTICLES PANEL -->
                    <div id="yourArticlesPanel" class="grid-column-left border-right">
                        <h5>Your Articles</h5>
                        <div id="articleTreeData" xmlns:x="http://www.w3.org/1999/xhtml">
                            {% set articleCollection = posts.CreatePostCollection(['Constraint' : 'contributor_id,category', 'Constraint_Type' : '1,1']) %}
                            <x:node category="Opinions - Politics">
                                {% for post in articleCollection %}
                                    <x:subnode title="{{ post.getTitle() }} [{{ post.getPublished(true) }}]">{{ post.getArticleRefId() }}</x:subnode>
                                {% endfor %}
                            </x:node>
                            {% set articleCollection = posts.CreatePostCollection(['Constraint' : 'contributor_id,category', 'Constraint_Type' : '1,2']) %}
                            <x:node category="Opinions - Science &amp; Technology">
                                {% for post in articleCollection %}
                                    <x:subnode title="{{ post.getTitle() }} [{{ post.getPublished(true) }}]">{{ post.getArticleRefId() }}</x:subnode>
                                {% endfor %}
                            </x:node>
                            {% set articleCollection = posts.CreatePostCollection(['Constraint' : 'contributor_id,category', 'Constraint_Type' : '1,3']) %}
                            <x:node category="Opinions - Health &amp; Medicine">
                                {% for post in articleCollection %}
                                    <x:subnode title="{{ post.getTitle() }} [{{ post.getPublished(true) }}]">{{ post.getArticleRefId() }}</x:subnode>
                                {% endfor %}
                            </x:node>
                            {% set articleCollection = posts.CreatePostCollection(['Constraint' : 'contributor_id,category', 'Constraint_Type' : '1,4']) %}
                            <x:node category="Opinions - Cryptocurrency">
                                {% for post in articleCollection %}
                                    <x:subnode title="{{ post.getTitle() }} [{{ post.getPublished(true) }}]">{{ post.getArticleRefId() }}</x:subnode>
                                {% endfor %}
                            </x:node>
                            {% set articleCollection = posts.CreatePostCollection(['Constraint' : 'contributor_id,category', 'Constraint_Type' : '1,5']) %}
                            <x:node category="Blurbs">
                                {% for post in articleCollection %}
                                    <x:subnode title="{{ post.getTitle() }} [{{ post.getPublished(true) }}]">{{ post.getArticleRefId() }}</x:subnode>
                                {% endfor %}
                            </x:node>
                            {% set articleCollection = posts.CreatePostCollection(['Constraint' : 'contributor_id,category', 'Constraint_Type' : '1,6']) %}
                            <x:node category="Humor">
                                {% for post in articleCollection %}
                                    <x:subnode title="{{ post.getTitle() }} [{{ post.getPublished(true) }}]">{{ post.getArticleRefId() }}</x:subnode>
                                {% endfor %}
                            </x:node>
                            {% set articleCollection = posts.CreatePostCollection(['Constraint' : 'contributor_id,category', 'Constraint_Type' : '1,7']) %}
                            <x:node category="Stories">
                                {% for post in articleCollection %}
                                    <x:subnode title="{{ post.getTitle() }} [{{ post.getPublished(true) }}]">{{ post.getArticleRefId() }}</x:subnode>
                                {% endfor %}
                            </x:node>
                            {% set articleCollection = posts.CreatePostCollection(['Constraint' : 'contributor_id,category', 'Constraint_Type' : '1,8']) %}
                            <x:node category="Interactive">
                                {% for post in articleCollection %}
                                    <x:subnode title="{{ post.getTitle() }} [{{ post.getPublished(true) }}]">{{ post.getArticleRefId() }}</x:subnode>
                                {% endfor %}
                            </x:node>
                            {% set articleCollection = posts.CreatePostCollection(['Constraint' : 'contributor_id,category', 'Constraint_Type' : '1,9']) %}
                            <x:node category="Site News">
                                {% for post in articleCollection %}
                                    <x:subnode title="{{ post.getTitle() }} [{{ post.getPublished(true) }}]">{{ post.getArticleRefId() }}</x:subnode>
                                {% endfor %}
                            </x:node>
                        </div>
                        <div class="article-create"><button id="article-create-button" type="button" class="btn btn-primary">+ Create New Article</button></div>
                        <div id="articleTree"></div>
                    </div>
                    <!-- EDITING AREA PANEL -->
                    <div id="editingAreaPanel" class="grid-column-main border-top border-bottom border-right border-left">
                        <h5>Editing Panel</h5>
                        <!-- QUILL TOOLBAR -->
                        <div id="quillToolbar"></div>
                        <!-- QUILL EDITOR -->
                        <div id="quillEditor" class="quill-editor"></div>
                        <br />
                        <div class="align-buttons-right">
                            <button id="submitChanges" disabled="disabled" class="disabled-button">Submit Changes</button>
                            <button id="cancelEdits" disabled="disabled" class="disabled-button">Cancel Edits</button>
                        </div>
                        <div id="article-settings">
                            <h3>Article Settings</h3>
                            <hr class="settings-divider" />
                            <div id="article-settings-accordion">
                                <h3>Image Archive</h3>
                                <div class="accordion-div">
                                    <!-- IMAGE ARCHIVE BUILDER -->
                                    <div class="row">
                                        <div class="col-lg-5">
                                            <h5>Images In Archive</h5>
                                            <button id="toggle-article-image-list">Collapse</button>
                                            <div id="article-image-list"></div>
                                        </div>
                                        <div class="col-lg-7">
                                            <h5>Add New Image</h5>
                                            <input id="upload-image" type="file" name="imgfile">
                                            <button id="browse-for-image-button" type="button" class="btn btn-primary">Browse...</button>
                                            <br />
                                            <br />
                                            <span class="smaller-text">Images can be in JPG, PNG, or GIF format. Please make sure that any images you choose to use in your article adheres to our <a href="{{ pathHeader }}/legal/policies/good-image-standards">good image standards policy</a> or it will not be approved for publishing.</span>
                                            <br />
                                            <br />
                                            <div id="upload-image-loading" class="clearfix">
                                                <img src="{{ pathHeader }}public/images/loading.gif" class="loading-img" />&nbsp;&nbsp; Processing Upload...
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <h3>Metadata</h3>
                                <div>

                                </div>
                                <h3>Publish</h3>
                                <div>

                                </div>
                            </div>
                        </div>
                        <!-- MODAL - IMAGE SELECTOR -->
                        <div id="modal-image-selector" title="Select Image to Insert">
                            <div id="modal-image-selector-inner">

                            </div>
                            <div id="modal-image-selector-altText">
                                <br /><strong>Alt Text</strong>
                                <form>
                                    <input type="text" title="Image Alt Text" id="imageAltText" disabled="disabled" />
                                </form>
                            </div>
                        </div>
                        <!-- MODAL - CREATE NEW ARTICLE -->
                        <div id="modal-create-new-article" title="Create New Article">
                            <form>
                                <strong>Article Title</strong><br /><input type="text" title="New Article Title" id="newArticleTitle" class="article-title-textbox" />
                                <br /><br />
                                <strong>Category</strong><br />
                                <select title="New Article Category" name="newArticleCategory">
                                    <option value="1">Opinion - Politics</option>
                                    <option value="2">Opinion - Science &amp; Technology</option>
                                    <option value="3">Opinion - Health &amp; Medicine</option>
                                    <option value="4">Opinion - Cryptocurrency</option>
                                    <option value="5">Blurbs</option>
                                    <option value="6">Humor</option>
                                    <option value="7">Stories</option>
                                    <option value="8">Interactive</option>
                                </select>
                            </form>
                            <span class="smaller-text">Please make sure that your article title adheres to our <a href="{{ pathHeader }}/legal/policies/good-article-title-standards">good article title standards policy.</a> or it will not be approved for publishing.</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        {# -- PAGE FOOTER -- #}
        {% include 'footer.volt' %}
        {# -- OUTPUT JS -- #}
        {{ assets.outputJs() }}
    </body>
</html>