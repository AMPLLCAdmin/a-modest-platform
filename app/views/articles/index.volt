<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>A Modest Platform - Article - {{ easy_title }}</title>
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="description" content="A Modest Platform is anything but modest, more amodest - the opposite of modest.
                                                Seeking to bring you compelling opinions, facts, humor, and fun activities from the turbulent world
                                                of current events.">
        {# -- OUTPUT CSS -- #}
        {{ assets.outputCss() }}
    </head>
    <body id="page-body">
        {# -- PAGE HEADER -- #}
        {% include 'header.volt' %}
        <div id="articles" class="page">
            {% set postMetadata = posts.fetchPostMetadata(ref_id) %}
            {% set postContent = posts.fetchPostContent(ref_id) %}
            <div class="grid-row">
                <div class="grid-column-main border-top border-bottom">
                    <div class="grid-column-submain-left">
                        PUBLISHED IN {{ postMetadata['Category']|uppercase }}
                    </div>
                    <div class="grid-column-submain-right">
                        PUBLISHED ON {{ postMetadata['PublishedTimestamp']|uppercase }}
                    </div>
                </div>
            </div>
            <div class="grid-row">
                <div class="grid-column-main border-right">
                    <h2>{{ postMetadata['Title'] }}</h2>
                    <hr />
                    {{ parser.parse(postContent, true) }}
                    <br />
                    <hr />
                    <span class="article-bottom">Written by {{ postMetadata['Contributor_Name'] }}</span>
                </div>
                <div class="grid-column-sponsored">
                    <h6>Sponsored</h6>
                </div>
            </div>
        </div>
        {# -- PAGE FOOTER -- #}
        {% include 'footer.volt' %}
        {# -- OUTPUT JS -- #}
        {{ assets.outputJs() }}
    </body>
</html>