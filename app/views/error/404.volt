<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>A Modest Platform - The Best Source for the Worst Kind of Info</title>
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="description" content="A Modest Platform is anything but modest, more amodest - the opposite of modest.
                                                    Seeking to bring you compelling opinions, facts, humor, and fun activities from the turbulent world
                                                    of current events.">
        {# -- OUTPUT CSS -- #}
        {{ assets.outputCss() }}
    </head>
    <body id="page-body">
        {# -- PAGE HEADER -- #}
        {% include 'header.volt' %}
        <div id="error" class="page">
            <div class="general-inner">
                <h4>404 Error! Resource Not Found</h4>
                <p>
                    What does this mean?
                    <br />
                    <br />
                    Please head on over to <a href="https://en.wikipedia.org/wiki/HTTP_404" target="_blank">this Wikipedia&reg; article</a> to learn more! Or you can <a href="javascript:history.back()">go back</a> to your previous page.
                    <br />
                    <br />
                    <strong>In the meantime, this error has been reported to the site administrators to improve your exprience<br />
                        and prevent this from happening in the future!</strong>
                    <br />
                    <br />
                    <span style="font-style: italic;">Note: Wikipedia&reg; is a registered trademark of the Wikimedia Foundation, Inc. All rights reserved.</span>
                </p>
            </div>
        </div>
        {# -- PAGE FOOTER -- #}
        {% include 'footer.volt' %}
        {# -- OUTPUT JS -- #}
        {{ assets.outputJs() }}
    </body>
</html>