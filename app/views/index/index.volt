<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>A Modest Platform - The Best Source for the Worst Kind of Info</title>
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="description" content="A Modest Platform is anything but modest, more amodest - the opposite of modest.
                                            Seeking to bring you compelling opinions, facts, humor, and fun activities from the turbulent world
                                            of current events.">
        {# -- OUTPUT CSS -- #}
        {{ assets.outputCss() }}
    </head>
    <body id="page-body">
        {# -- PAGE HEADER -- #}
        {% include 'header.volt' %}
        <div id="index" class="page">
            <div class="grid-row">
                <div class="grid-column-highlight">
                    {% set highlightCollection = posts.createPostCollection(['Constraint' : 'highlight,published', 'Constraint_Type' : '1,1']) %}
                    {% for post in highlightCollection %}
                        {% set articleRefId = post.getArticleRefId() %}
                        {% set postMetadata = posts.fetchPostMetadata(articleRefId) %}
                        <a class="post-container container-fluid" href="articles/refId:{{ articleRefId }}/title:{{ post.getURLFriendlyTitle() }}">
                            <div class="img-container">
                                <img src="{{ pathHeader }}image-server/image.php?refid={{ articleRefId }}&type=highlight" class="img-responsive" />
                            </div>
                            <h4>{{ post.getTitle() }}</h4>
                            <span class="infoTextProminent">
                                From {{ postMetadata['Category'] }}
                            </span>
                        </a>
                    {% endfor %}
                </div>
                <div class="grid-column-spotlight">
                    {% set spotlightCollection = posts.createPostCollection(['Constraint' : 'spotlight,published', 'Constraint_Type' : '1,1']) %}
                    {% for post in spotlightCollection %}
                        {% set articleRefId = post.getArticleRefId() %}
                        {% set postMetadata = posts.fetchPostMetadata(articleRefId) %}
                        <a class="post-container container-fluid" href="articles/refId:{{ articleRefId }}/title:{{ post.getURLFriendlyTitle() }}">
                            <div class="img-container">
                                <img src="{{ pathHeader }}image-server/image.php?refid={{ articleRefId }}&type=highlight" class="img-responsive" />
                            </div>
                            <h3>{{ post.getTitle() }}</h3>
                            <span class="postContent">
                                {{ parser.removeTags(posts.fetchPostContent(articleRefId, true, true)) }}
                            </span>
                            <br />
                            <span class="infoText">
                                Posted in {{ postMetadata['Category'] }} by {{ postMetadata['Contributor_Name'] }}
                            </span>
                        </a>
                    {% endfor %}
                </div>
                <div class="grid-column-sponsored">

                </div>
            </div>
        </div>
        {# -- PAGE FOOTER -- #}
        {% include 'footer.volt' %}
        {# -- OUTPUT JS -- #}
        {{ assets.outputJs() }}
    </body>
</html>