<?php
namespace AModestPlatform;

/* -- PROTOTYPE CLASS Config -- */
final class _configPrototype
{
    /**
     * @var $database _config_Database
     */
    public $database;

    /**
     * @var $directories _config_Directories
     */
    public $directories;

    /**
     * @var $url _config_URL
     */
    public $url;

    /**
     * @var $application _config_Application
     */
    public $application;

    /**
     * @var $acl _config_ACL
     */
    public $acl;
}

/* -- PROTOTYPE SUBCLASS Config-Database -- */
final class _config_Database
{
    public $host;
    public $username;
    public $password;
    public $dbname;
}

/* -- PROTOTYPE SUBCLASS Config-Directories -- */
final class _config_Directories
{
    public $serverURI;
    public $baseURI;
    public $appDir;
    public $modelsDir;
    public $controllersDir;
    public $viewsDir;
    public $pluginsRootDir;
    public $pluginsHelpersDir;
    public $pluginsManagersDir;
    public $formsDir;
    public $publicDir;
    public $stylesDir;
    public $imagesDir;
    public $jsDir;
    public $libDir;
    public $voltCompiledDir;
    public $configDir;
}

/* -- PROTOTYPE SUBCLASS Config-URL -- */
final class _config_URL
{
    public $redirectPathHeader;
}

/* -- PROTOTYPE SUBCLASS Config-Application -- */
final class _config_Application
{
    public $defaultControllerNamespace;
    public $alwaysCompileVolt;
    public $isReporting;
    public $imageServerPath;
}

/* -- PROTOTYPE SUBCLASS Config-ACL -- */
final class _config_ACL
{
    public $L1;
    public $L2;
    public $L3;
    public $L4;
    public $L5;
    public $L6;
}