<?php
namespace AModestPlatform\Plugins\Managers;

/* -- DEPENDENCIES -- */
// PHP
use \DateTime;
use \Exception;

// Phalcon
use Phalcon\Di;
use Phalcon\Http\RequestInterface;
use Phalcon\Mvc\User\Component;
use Phalcon\Session\Adapter\Files as SessionAdapter;

// Internal
use AModestPlatform\Models\User;
use AModestPlatform\Models\Contributor;
use AModestPlatform\Models\UserCognizance;

/* -- CLASS UserManager -- */
class UserManager extends Component
{
    /* -- PROTECTED -- */
    /** @var SessionAdapter $session */
    protected $session;

    /* -- CONSTRUCTOR -- */
    /**
     * UserManager constructor.
     * @param $di Di Phalcon DependencyInjector instance
     */
    public function __construct($di)
    {
        $this->setDI($di);
        $this->session = $di->get('session');
    }

    /* -- PUBLIC CLASS METHODS -- */
    // Cognizant
    public function cognizant()
    {
        // First check if we have an active session
        if ($this->checkCognizanceSession())
        {
            $cog = $this->createCognizanceFromSession();
            if (!$cog)
            {
                $this->destroyCognizance();
                return false;
            }
            return [
                "Result" => true,
                "User" => $cog
            ];
        }

        // See if the user has a cookie for an active session
        if ($this->checkCognizanceCookies())
        {
            $cog = $this->createCognizanceFromCookies();
            if (!$cog)
            {
                $this->destroyCognizance();
                return false;
            }
            return [
                "Result" => true,
                "User" => $cog
            ];
        }

        return false;
    }

    // Login
    public function login($credentials)
    {
        return $this->checkCredentialsInit($credentials);
    }

    // Logout
    public function logout()
    {
        return $this->destroyCognizance();
    }

    // Register
    public function register($request)
    {
        return $this->createNewFromRequestData($request);
    }

    /* -- PRIVATE CLASS METHODS -- */
    // Check Credentials Initial
    protected function checkCredentialsInit($credentials)
    {
        // Set Globals
        /** @var User $user */
        $user = null;
        $userFound = false;
        $passwordValid = false;
        $cognizant = false;

        $id = isset($credentials['id']) ? $credentials['id'] : null;
        if (!is_null($id) && !$userFound)
        {
            // Find the user
            if (filter_var($id, FILTER_VALIDATE_EMAIL))
            {
                try
                {
                    $user = User::findFirst("email = '$id'");
                    if ($user)
                    {
                        $userFound = true;
                    }
                    else
                    {
                        return [false, "Email was not located in the database!"];
                    }
                }
                catch (Exception $e)
                {
                    return [false, "Email was not located in the database!"];
                }
            }
            else
            {
                try
                {
                    $user = User::findFirst("username = '$id'");
                    if ($user)
                    {
                        $userFound = true;
                    }
                    else
                    {
                        return [false, "Username was not located in database!"];
                    }
                }
                catch (Exception $e)
                {
                    return [false, "Username was not located in database!"];
                }
            }
        }


        // If the user is found, check the password
        if ($userFound)
        {
            $password = isset($credentials['password']) ? $credentials['password'] : null;
            if (!is_null($password))
            {
                if ($this->security->checkHash(ENCRYPTION_KEY . $password, $user->getPassword()))
                {
                    $passwordValid = true;
                }
                else
                {
                    return [false, "Password was invalid!"];
                }
            }
        }
        else
        {
            return [false, "User was not found for password comparison!"];
        }

        // GREAT SUCCESS...OR NO?
        if ($userFound && $passwordValid)
        {
            // Get our username
            $username = $user->getUsername();

            // If "Remember Me" was checked, initiate cognizance
            if (isset($credentials['rememberMe']))
            {
                if ($credentials['rememberMe'] == "yes")
                {
                    // Get our e-mail
                    $email = $user->getEmail();

                    if ($this->initiateCognizance($username, $email))
                    {
                        $cognizant = true;
                    }
                }
            }

            // Create and update session ID
            $now = new DateTime();
            $mysql_now = $now->format('Y-m-d H:i:s');
            $sess_id = $this->security->hash(ENCRYPTION_KEY . $username . $now->getTimestamp());
            $user->setSessionId($sess_id);
            $user->setLastLogin($mysql_now);
            $user->save();

            // Create our session
            $this->session->set('AMP_AUTH_IDENTITY', [
                'id'            => $sess_id,
                'username'      => $username
            ]);

            return [true, $cognizant, $sess_id];
        }
        else
        {
            return [false, "User and/or password was invalid!"];
        }
    }

    // Initiate Cognizance
    protected function initiateCognizance($username, $email)
    {
        $token = md5($username . $email);

        $now = new DateTime();
        $expiry = $now->modify("+7 days");
        $mysql_expiry = $expiry->format('Y-m-d H:i:s');

        $cognizance = new UserCognizance();
        $cognizance->setToken($token);
        $cognizance->setExpiry($mysql_expiry);
        $cognizance->save();

        if ($cognizance->save())
        {
            $this->cookies->set('AMP_USERNAME', $username, (time()+(60*60*24*7)));
            $this->cookies->set('AMP_TOKEN', $token, (time()+(60*60*24*7)));

            return true;
        }
        else
        {
            return false;
        }
    }

    // Cognizance Exists
    protected function checkCognizanceCookies()
    {
        return $this->cookies->has("AMP_TOKEN");
    }

    // Cognizance-Session Exists
    protected function checkCognizanceSession()
    {
        return $this->session->has("AMP_AUTH_IDENTITY");
    }

    // Create Cognizance From Session
    protected function createCognizanceFromSession()
    {
        // Get our identity
        $identity = $this->session->get('AMP_AUTH_IDENTITY');

        // Check to make sure the cognizance is not expired
        if (isset($identity['id']))
        {
            $user = User::findFirst("username = '{$identity['username']}'");
            return $user;
        }
        else
        {
            return false;
        }
    }

    // Create Cognizance From Cookies
    protected function createCognizanceFromCookies()
    {
        // Get our cookies
        $uname = $this->cookies->get("AMP_USERNAME")->getValue();
        $token = $this->cookies->get("AMP_TOKEN")->getValue();

        /** @var User $user */
        $user = User::findFirst("username = '$uname'");
        if ($user)
        {
            $genToken = md5($user->getUsername() . $user->getEmail());

            if ($token == $genToken)
            {
                $cognizantUser = UserCognizance::findFirst("token = '$token'");
                if ($cognizantUser)
                {
                    // Delete the old cognizant user
                    $cognizantUser->delete();

                    // Create new session
                    $now = new DateTime();
                    $mysql_now = $now->format('Y-m-d H:i:s');

                    // Check if there is already a session or if we should create a new one
                    if (!$this->checkCognizanceSession())
                    {
                        $sess_id = $this->security->hash(ENCRYPTION_KEY . $user->getUsername() . $now->getTimestamp());
                        $this->session->set('AMP_AUTH_IDENTITY', [
                            'id'            => $sess_id,
                            'username'      => $user->getUsername()
                        ]);
                    }

                    // Refresh our cognizance in the database
                    $newCognizantUser = new UserCognizance();
                    $newCognizantUser->setToken($token);
                    $newCognizantUser->setExpiry($mysql_now);
                    $newCognizantUser->save();

                    return $user;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }
        else
        {
            return false;
        }
    }

    // Create New From Request Data
    /**
     * @param $requestData RequestInterface The request interface to fetch post data from
     * @return bool
     */
    protected function createNewFromRequestData(&$requestData)
    {
        $now = new DateTime();
        $mysql_now = $now->format('Y-m-d H:i:s');

        $uname = $requestData->getPost('username', 'striptags');

        $newUser = new User();
        $newUser->setUsername($uname);
        $newUser->setPassword($this->security->hash(ENCRYPTION_KEY . $requestData->getPost('password')));
        $newUser->setEmail($requestData->getPost('email'));
        $newUser->setPermissionLevel('L1');
        $newUser->setSessionId($this->security->hash(ENCRYPTION_KEY . $uname . $now->getTimestamp()));
        $newUser->setLastLogin($mysql_now);

        // Check if they wanted to be a contributor
        if ($requestData->getPost('checkContributor') == "yes" && $requestData->getPost('fullName') != "")
        {
            $newContributor = new Contributor();
            $newContributor->setName($requestData->getPost('fullName'));
            if ($newContributor->save())
            {
                // Now get our contributor ID
                /**
                 * @var $contributor Contributor
                 */
                $contributor = Contributor::findFirst("name = '{$newContributor->getName()}'");
                if ($contributor)
                {
                    $newUser->setContributorId($contributor->getID());
                }
            }
        }
        else
        {
            $newUser->setContributorId(0);
        }

        return $newUser->save();
    }

    // Destroy Cognizance (Analogous to Logout)
    protected function destroyCognizance()
    {
        $success = false;

        try {
            // Check if we have a session
            if ($this->checkCognizanceSession())
            {
                $this->session->destroy("AMP_AUTH_IDENTITY");
            }

            // Check if we have cookies
            if ($this->checkCognizanceCookies())
            {
                $uname = $this->cookies->get("AMP_USERNAME")->getValue();
                $token = $this->cookies->get("AMP_TOKEN")->getValue();

                /** @var User $user */
                $user = User::findFirst("username = '$uname'");
                if ($user)
                {
                    $genToken = md5($user->getUsername() . $user->getEmail());

                    if ($genToken == $token)
                    {
                        $cognizance = UserCognizance::findFirst("token = '$token'");
                        if ($cognizance)
                        {
                            $cognizance->delete();
                        }
                    }
                }

                // Delete our cookies
                $this->cookies->delete("AMP_USERNAME");
                $this->cookies->delete("AMP_TOKEN");

                $success['Debug_CookiesDestroyed'] = "Success";
            }
            else
            {
                $success['Debug_CookiesDestroyed'] = "Irrelevant";
            }

            $success['Result'] = true;
            return $success;
        }
        catch (Exception $e)
        {
            return false;
        }
    }
}