<?php
namespace AModestPlatform\Plugins\Managers;

/* -- DEPENDENCIES -- */
// Phalcon

use Phalcon\Di;
use Phalcon\Mvc\User\Component;

// Internal
use AModestPlatform\_configPrototype;
use AModestPlatform\Models\Article;
use AModestPlatform\Plugins\Helpers\ArticleParsingHelper;

/* -- CLASS AxiosManager -- */
// NOTE: This class extends the Phalcon injectable Component class but currently is not shared through the global
// DependencyInjector but is instead only used as a mechanism to formulate responses for axios requests
class AxiosManager extends Component
{
    /* -- PROTECTED -- */
    /** @var $posts PostsManager */
    protected $posts;

    /** @var $articleParser ArticleParsingHelper */
    protected $articleParser;

    /** @var $config _configPrototype */
    protected $config;

    /* -- CONSTRUCTOR -- */
    /** @param $di Di */
    public function __construct($di)
    {
        $this->setDI($di);
        $this->posts = $di->get('postsManager');
        $this->articleParser = $di->get('articleParser');
        $this->config = $di->get('config');
    }

    /* -- PUBLIC CLASS METHODS -- */
    /* --- CONTRIBUTOR PANEL ACTIONS --- */
    // Get Article Content
    public function getArticleContent($articleRefId, $isReverseParsed = true)
    {
        $rawContent = $this->posts->fetchPostContent($articleRefId);
        return $this->articleParser->parse($rawContent, $isReverseParsed);
    }

    // Update Article Content
    public function updateArticleContent($articleRefId, $submittedContent)
    {
        /** @var Article $article */
        $article = Article::findFirst("ref_id = '$articleRefId'");

        $article->setRefId($articleRefId);
        $article->setContent($this->articleParser->parse($submittedContent));

        $isSaved = $article->save();
        if ($isSaved === false)
        {
            return $article->getMessages();
        }
        else
        {
            return $isSaved;
        }
    }

    // Get Image Archive List
    public function getImageArchiveList($articleRefId)
    {
        // Fetch data using cURL from the image server
        $fetchURL = $this->config->application->imageServerPath . "/data.php?refid=$articleRefId&type=archiveList";
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_URL, $fetchURL);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        $result = curl_exec($ch);
        curl_close($ch);

        $data = json_decode($result);
        $fileCount = count($data);

        if ($fileCount > 0)
        {
            $html = "<ul class='list-group img-archive-list'>";
            $html .= "<li class='list-group-item ia-list-item' id='highlight'><span class='badge'>0</span>Highlight Image</li>";
            for ($i = 0; $i < $fileCount; $i++) {
                $html .= "<li class='list-group-item ia-list-item' id='$i'>";
                $html .= "<span class='badge'>" . ($i + 1) . "</span>";
                $html .= $data[$i];
                $html .= "</li>";
            }
            $html .= "</ul>";
        }
        else
        {
            $html = "<ul class='list-group img-archive-list'>";
            $html .= "<li class='list-group-item ia-list-item' id='highlight'><span class='badge'>0</span>Highlight Image</li>";
            $html .= "</ul>";
        }

        return $html;
    }
}