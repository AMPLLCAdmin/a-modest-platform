<?php
namespace AModestPlatform\Plugins\Managers;

/* -- DEPENDENCIES -- */
// Phalcon
use Phalcon\Mvc\User\Component;

//Internal
use AModestPlatform\Models\Post;
use AModestPlatform\Models\Article;
use AModestPlatform\Models\Category;
use AModestPlatform\Models\Contributor;

/* -- CLASS PostsManager -- */
class PostsManager extends Component
{
    /* -- CONSTRUCTOR -- */
    public function __construct($di)
    {
        $this->setDI($di);
    }

    /* -- PUBLIC CLASS METHODS -- */
    // Fetch Post Metadata
    public function fetchPostMetadata($refId)
    {
        /** @var Post $post */
        $post = $this->getPost($refId);
        if (!$post)
        {
            return false;
        }
        else
        {
            return array(
                "RefId"                 => $post->getArticleRefId(),
                "Title"                 => $post->getTitle(),
                "PublishedTimestamp"    => $post->getPostedDate(),
                "Category"              => $this->getMetadata(["Category", $post->getCategory()]),
                "Contributor_Name"      => $this->getMetadata(["Contributor", "Name", $post->getContributorId()]),
                "Contributor_Bio"       => $this->getMetadata(["Contributor", "Bio", $post->getContributorId()])
            );
        }
    }

    // Create Post Collection
    public function createPostCollection($options = array())
    {
        if (is_array($options))
        {
            // Get Constraint(s)
            $constraint = $options['Constraint'] ?? false;
            if (!$constraint) { return false; }
            else
            {
                $constraints = explode(",", $constraint);
            }

            // Get Constraint Type(s)
            $constraint_type = $options['Constraint_Type'] ?? false;
            if (!$constraint_type) { return false; }
            else
            {
                $constraint_types = explode(",", $constraint_type);
            }

            //Build Condition String and Bound-Array
            $conditionStr = "";
            $conditionBoundArray = [];
            for ($i = 0; $i < count($constraints); $i++)
            {
                $trueVal = $i + 1;
                $conditionStr .= $constraints[$i] . "= ?$trueVal AND ";
                $conditionBoundArray[$trueVal] = $constraint_types[$i];
            }
            $conditionSubstr = substr($conditionStr, 0, -5);

            $collection = Post::find([
                'conditions' => $conditionSubstr,
                'bind' => $conditionBoundArray
            ]);

            return $collection;
        }

        return false;
    }

    // Fetch Post Content
    public function fetchPostContent($articleRefId, $truncateLen = false, $stripTags = false, $truncateLenRatio = 0.15)
    {
        if (!$truncateLen)
        {
            try
            {
                /** @var Article $article */
                $article = $this->getArticle($articleRefId);
                if (!$stripTags)
                {
                    return $article->getContent();
                }
                else
                {
                    return strip_tags($article->getContent());
                }
            }
            catch(\Exception $e)
            {
                return false;
            }

        }
        else
        {
            try
            {
                /** @var Article $article */
                $article = $this->getArticle($articleRefId);
                $dynTruncLen = strlen($article->getContent()) * $truncateLenRatio;
                $content_trunc = substr($article->getContent(), 0, $dynTruncLen) . "...";
                if (!$stripTags)
                {
                    return $content_trunc;
                }
                else
                {
                    return strip_tags($content_trunc);
                }
            }
            catch(\Exception $e)
            {
                return false;
            }
        }
    }

    /* -- PRIVATE CLASS METHODS -- */
    // Get Post (Checks if Exists)
    protected function getPost($refId)
    {
        try
        {
            $post = Post::findFirst("article_ref_id = '$refId'");
        }
        catch (\Exception $e)
        {
            return false;
        }
        return $post;
    }

    // Get Article
    protected function getArticle($refId)
    {
        try
        {
            $article = Article::findFirst("ref_id = '$refId'");
        }
        catch (\Exception $e)
        {
            return false;
        }
        return $article;
    }

    // Get Metadata
    protected function getMetadata($metadata_fetch = null)
    {
        if (is_array($metadata_fetch))
        {
            if ($metadata_fetch[0] === "Category")
            {
                /** @noinspection PhpUndefinedMethodInspection */
                return Category::findFirst($metadata_fetch[1])->getCategoryName();
            }
            else if ($metadata_fetch[0] === "Contributor")
            {
                if ($metadata_fetch[1] === "Name")
                {
                    /** @noinspection PhpUndefinedMethodInspection */
                    return Contributor::findFirst($metadata_fetch[2])->getName();
                }
                else if ($metadata_fetch[1] === "Bio")
                {
                    /** @noinspection PhpUndefinedMethodInspection */
                    return Contributor::findFirst($metadata_fetch[2])->getBio();
                }
                else if ($metadata_fetch[1] === "Pic")
                {
                    /** @noinspection PhpUndefinedMethodInspection */
                    return Contributor::findFirst($metadata_fetch[2])->getPic();
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }
        else
        {
            return false;
        }
    }
}