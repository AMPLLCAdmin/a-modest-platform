<?php
namespace AModestPlatform\Plugins\Helpers;

/* -- DEPENDENCIES -- */
// Phalcon
use Phalcon\Mvc\Dispatcher;
use Phalcon\Mvc\User\Component;
use Phalcon\Events\Manager as EventsManager;

/* -- CLASS EventsHelper -- */

class EventsHelper extends Component
{
    /* -- PROTECTED -- */
    /** @var EventsManager $em */
    protected $em;

    /* -- STATIC -- */
    /** @var EventsManager $EventsManager */
    static $EventsManager;

    /* -- CONSTRUCTOR -- */
    public function __construct($di)
    {
        $this->setDI($di);

        // Setup Events Manager
        $eventsManager = new EventsManager();
        $this->em = &$eventsManager;

        // Attach to static
        $this->attachToStatic();
    }

    /* -- PUBLIC FUNCTIONS -- */
    // Attach to Static
    public function attachToStatic()
    {
        EventsHelper::$EventsManager = $this->em;
    }

    // Init Error Handling
    public function initErrorHandling($types = array(
        '404' => ['error', 'handle404']
    ))
    {
        $this->em->attach(
            'dispatch:beforeException',
            function($event, $dispatcher, $exception) use ($types) {
                switch ($exception->getCode())
                {
                    case Dispatcher::EXCEPTION_HANDLER_NOT_FOUND:
                    case Dispatcher::EXCEPTION_ACTION_NOT_FOUND:
                        $dispatcher->forward([
                            'controller' => $types['404'][0],
                            'action' => $types['404'][1]
                        ]);
                        return false;
                }
                return true;
            }
        );
    }

    // Init Parameter Handling
    public function initParameterHandling()
    {
        $this->em->attach(
            'dispatch:beforeDispatchLoop',
            function ($event, $dispatcher) {
                $params = $dispatcher->getParams();

                $keyParams = [];

                // Explode each parameter as key,value pairs
                foreach ($params as $number => $value) {
                    $parts = explode(':', $value);

                    $keyParams[$parts[0]] = $parts[1];
                }

                // Override parameters
                $dispatcher->setParams($keyParams);
            }
        );
    }
}