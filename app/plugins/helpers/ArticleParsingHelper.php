<?php
namespace AModestPlatform\Plugins\Helpers;

/* -- DEPENDENCIES -- */
// Phalcon
use AModestPlatform\Models\Article;
use Phalcon\Mvc\User\Component;

// PHPDoc
use AModestPlatform\_configPrototype;

/* -- CLASS ArticleParsingHelper -- */
/**
 * @property _configPrototype $config
 */
class ArticleParsingHelper extends Component
{
    /* -- CONSTANTS -- */
    public const REGEX_PROPER_TAG_FORMAT          = "/\[\[(.*?)\]\] ?/";

    /* -- TAG DEFINITIONS -- */
    /*
     * NOTE: Generally speaking, regex is used to detect and replace tag formats both to and from
     * HTML. However, some tags are so simple that regex is not really used to find and replace them
     * (but rather, str_replace is used). However, naming conventions in this section still dictatate
     * that the tag start with REGEX_ just for consistency
     */
    //<p> Tag
    public const REGEX_TAG_P = [
        "OPEN"                  => "/<p(.*)>/U",
        "CLOSE"                 => "</p>",
        "REPLACE_OPEN"          => "[[p$1]]",
        "REPLACE_CLOSE"         => "[[/p]]",
        "REVERSE_OPEN"          => "/\[\[p(.*)\]\]/U",
        "REVERSE_REPLACE_OPEN"  => "<p$1>"
    ];

    //<strong> Tag
    public const REGEX_TAG_STRONG = [
        "OPEN"          => "<strong>",
        "CLOSE"         => "</strong>",
        "REPLACE_OPEN"  => "[[strong]]",
        "REPLACE_CLOSE" => "[[/strong]]"
    ];

    //<br> Tag
    public const REGEX_TAG_BR = [
        "OPEN"          => "<br>",
        "REPLACE_OPEN"  => "[[br]]"
    ];

    //<h*> Tag
    public const REGEX_TAG_H = [
        "OPEN"                  => "/<h([0-6])(.*)>/U",
        "CLOSE"                 => "/<\/h([0-6])>/U",
        "REPLACE_OPEN"          => "[[h$1$2]]",
        "REPLACE_CLOSE"         => "[[/h$1]]",
        "REVERSE_OPEN"          => "/\[\[h([0-6])(.*)\]\]/U",
        "REVERSE_CLOSE"         => "/\[\[\/h([0-6])\]\]/U",
        "REVERSE_REPLACE_OPEN"  => "<h$1$2>",
        "REVERSE_REPLACE_CLOSE" => "</h$1>"
    ];

    //<em> Tag (italics)
    public const REGEX_TAG_EM = [
        "OPEN"          => "<em>",
        "CLOSE"         => "</em>",
        "REPLACE_OPEN"  => "[[em]]",
        "REPLACE_CLOSE" => "[[/em]]"
    ];

    //<u> Tag
    public const REGEX_TAG_U = [
        "OPEN"          => "<u>",
        "CLOSE"         => "</u>",
        "REPLACE_OPEN"  => "[[u]]",
        "REPLACE_CLOSE" => "[[/u]]"
    ];

    //<s> Tag
    public const REGEX_TAG_S = [
        "OPEN"          => "<s>",
        "CLOSE"         => "</s>",
        "REPLACE_OPEN"  => "[[s]]",
        "REPLACE_CLOSE" => "[[/s]]"
    ];

    //<a> Tag (link)
    public const REGEX_TAG_LINK = [
        "OPEN"                  => "/<a(.*)>/U",
        "CLOSE"                 => "</a>",
        "REPLACE_OPEN"          => "[[a$1]]",
        "REPLACE_CLOSE"         => "[[/a]]",
        "REVERSE_OPEN"          => "/\[\[a(.*)\]\]/U",
        "REVERSE_REPLACE_OPEN"  => "<a$1>"
    ];

    //<img> Tag
    public const REGEX_TAG_IMG = [
        "OPEN"                  => "/\<img alt\=\"(.*)\" src\=\"(.*)\" class\=\"(.*)\" style\=\"(.*)\">/U",
        "REPLACE_OPEN"          => "[[img alt=\"$1\" src=\"$2\" class=\"$3\" style=\"$4\"]]",
        "REVERSE_OPEN"          => "/\[\[img alt\=\"(.*)\" src\=\"(.*)\" class\=\"(.*)\" style\=\"(.*)\"\]\]/U",
        "REVERSE_REPLACE_OPEN"  => "<img alt=\"$1\" src=\"$2\" class=\"$3\" style=\"$4\">"
    ];

    /* -- CONSTRUCTOR -- */
    public function __construct($di)
    {
        //Nullable - Allows the article parser to be used in the playground environment
        if ($di != null)
        {
            $this->setDI($di);
        }
    }

    /* -- PUBLIC CLASS METHODS -- */
    // Remove (Proper) Tags
    public function removeTags($content)
    {
        // Parse Image Tags
        $parsedContent = preg_replace(
            ArticleParsingHelper::REGEX_PROPER_TAG_FORMAT,
            "",
            $content);

        // Return
        return $parsedContent;
    }

    // Convert HTML to Proper Tags
    public function parse($content, $reverse = false)
    {
        $parsedContent = $content;
        if (!$reverse)
        {
            // Parse <p> tags
            $parsedContent = preg_replace(
                ArticleParsingHelper::REGEX_TAG_P["OPEN"],
                ArticleParsingHelper::REGEX_TAG_P["REPLACE_OPEN"],
                $parsedContent
            );
            $parsedContent = str_replace(
                ArticleParsingHelper::REGEX_TAG_P["CLOSE"],
                ArticleParsingHelper::REGEX_TAG_P["REPLACE_CLOSE"],
                $parsedContent
            );

            // Parse <strong> tags
            $parsedContent = str_replace(
                ArticleParsingHelper::REGEX_TAG_STRONG["OPEN"],
                ArticleParsingHelper::REGEX_TAG_STRONG["REPLACE_OPEN"],
                $parsedContent
            );
            $parsedContent = str_replace(
                ArticleParsingHelper::REGEX_TAG_STRONG["CLOSE"],
                ArticleParsingHelper::REGEX_TAG_STRONG["REPLACE_CLOSE"],
                $parsedContent
            );

            // Parse <br> tags
            $parsedContent = str_replace(
                ArticleParsingHelper::REGEX_TAG_BR["OPEN"],
                ArticleParsingHelper::REGEX_TAG_BR["REPLACE_OPEN"],
                $parsedContent
            );

            // Parse <h*> tags
            $parsedContent = preg_replace(
                ArticleParsingHelper::REGEX_TAG_H["OPEN"],
                ArticleParsingHelper::REGEX_TAG_H["REPLACE_OPEN"],
                $parsedContent
            );
            $parsedContent = preg_replace(
                ArticleParsingHelper::REGEX_TAG_H["CLOSE"],
                ArticleParsingHelper::REGEX_TAG_H["REPLACE_CLOSE"],
                $parsedContent
            );

            // Parse <em> tags
            $parsedContent = str_replace(
                ArticleParsingHelper::REGEX_TAG_EM["OPEN"],
                ArticleParsingHelper::REGEX_TAG_EM["REPLACE_OPEN"],
                $parsedContent
            );
            $parsedContent = str_replace(
                ArticleParsingHelper::REGEX_TAG_EM["CLOSE"],
                ArticleParsingHelper::REGEX_TAG_EM["REPLACE_CLOSE"],
                $parsedContent
            );

            // Parse <u> tags
            $parsedContent = str_replace(
                ArticleParsingHelper::REGEX_TAG_U["OPEN"],
                ArticleParsingHelper::REGEX_TAG_U["REPLACE_OPEN"],
                $parsedContent
            );
            $parsedContent = str_replace(
                ArticleParsingHelper::REGEX_TAG_U["CLOSE"],
                ArticleParsingHelper::REGEX_TAG_U["REPLACE_CLOSE"],
                $parsedContent
            );

            // Parse <s> tags
            $parsedContent = str_replace(
                ArticleParsingHelper::REGEX_TAG_S["OPEN"],
                ArticleParsingHelper::REGEX_TAG_S["REPLACE_OPEN"],
                $parsedContent
            );
            $parsedContent = str_replace(
                ArticleParsingHelper::REGEX_TAG_S["CLOSE"],
                ArticleParsingHelper::REGEX_TAG_S["REPLACE_CLOSE"],
                $parsedContent
            );

            // Parse <a> (link) tags
            $parsedContent = preg_replace(
                ArticleParsingHelper::REGEX_TAG_LINK["OPEN"],
                ArticleParsingHelper::REGEX_TAG_LINK["REPLACE_OPEN"],
                $parsedContent
            );
            $parsedContent = str_replace(
                ArticleParsingHelper::REGEX_TAG_LINK["CLOSE"],
                ArticleParsingHelper::REGEX_TAG_LINK["REPLACE_CLOSE"],
                $parsedContent
            );

            // Parse <img> tags
            $parsedContent = preg_replace(
                ArticleParsingHelper::REGEX_TAG_IMG["OPEN"],
                ArticleParsingHelper::REGEX_TAG_IMG["REPLACE_OPEN"],
                $parsedContent
            );

            // Strip all other HTML tags
            $parsedContent = strip_tags($parsedContent);

            return $parsedContent;
        }
        else
        {
            // Parse [[p]] tags
            $parsedContent = preg_replace(
                ArticleParsingHelper::REGEX_TAG_P["REVERSE_OPEN"],
                ArticleParsingHelper::REGEX_TAG_P["REVERSE_REPLACE_OPEN"],
                $parsedContent
            );
            $parsedContent = str_replace(
                ArticleParsingHelper::REGEX_TAG_P["REPLACE_CLOSE"],
                ArticleParsingHelper::REGEX_TAG_P["CLOSE"],
                $parsedContent
            );

            // Parse [[strong]] tags
            $parsedContent = str_replace(
                ArticleParsingHelper::REGEX_TAG_STRONG["REPLACE_OPEN"],
                ArticleParsingHelper::REGEX_TAG_STRONG["OPEN"],
                $parsedContent
            );
            $parsedContent = str_replace(
                ArticleParsingHelper::REGEX_TAG_STRONG["REPLACE_CLOSE"],
                ArticleParsingHelper::REGEX_TAG_STRONG["CLOSE"],
                $parsedContent
            );

            // Parse [[br]] tags
            $parsedContent = str_replace(
                ArticleParsingHelper::REGEX_TAG_BR["REPLACE_OPEN"],
                ArticleParsingHelper::REGEX_TAG_BR["OPEN"],
                $parsedContent
            );

            // Parse [[h*]] tags
            $parsedContent = preg_replace(
                ArticleParsingHelper::REGEX_TAG_H["REVERSE_OPEN"],
                ArticleParsingHelper::REGEX_TAG_H["REVERSE_REPLACE_OPEN"],
                $parsedContent
            );
            $parsedContent = preg_replace(
                ArticleParsingHelper::REGEX_TAG_H["REVERSE_CLOSE"],
                ArticleParsingHelper::REGEX_TAG_H["REVERSE_REPLACE_CLOSE"],
                $parsedContent
            );

            // Parse [[em]] tags
            $parsedContent = str_replace(
                ArticleParsingHelper::REGEX_TAG_EM["REPLACE_OPEN"],
                ArticleParsingHelper::REGEX_TAG_EM["OPEN"],
                $parsedContent
            );
            $parsedContent = str_replace(
                ArticleParsingHelper::REGEX_TAG_EM["REPLACE_CLOSE"],
                ArticleParsingHelper::REGEX_TAG_EM["CLOSE"],
                $parsedContent
            );

            //Parse [[u]] tags
            $parsedContent = str_replace(
                ArticleParsingHelper::REGEX_TAG_U["REPLACE_OPEN"],
                ArticleParsingHelper::REGEX_TAG_U["OPEN"],
                $parsedContent
            );
            $parsedContent = str_replace(
                ArticleParsingHelper::REGEX_TAG_U["REPLACE_CLOSE"],
                ArticleParsingHelper::REGEX_TAG_U["CLOSE"],
                $parsedContent
            );

            //Parse [[s]] tags
            $parsedContent = str_replace(
                ArticleParsingHelper::REGEX_TAG_S["REPLACE_OPEN"],
                ArticleParsingHelper::REGEX_TAG_S["OPEN"],
                $parsedContent
            );
            $parsedContent = str_replace(
                ArticleParsingHelper::REGEX_TAG_S["REPLACE_CLOSE"],
                ArticleParsingHelper::REGEX_TAG_S["CLOSE"],
                $parsedContent
            );

            // Parse [[a]] (link) tags
            $parsedContent = preg_replace(
                ArticleParsingHelper::REGEX_TAG_LINK["REVERSE_OPEN"],
                ArticleParsingHelper::REGEX_TAG_LINK["REVERSE_REPLACE_OPEN"],
                $parsedContent
            );
            $parsedContent = str_replace(
                ArticleParsingHelper::REGEX_TAG_LINK["REPLACE_CLOSE"],
                ArticleParsingHelper::REGEX_TAG_LINK["CLOSE"],
                $parsedContent
            );

            // Parse [[img]] tags
            $parsedContent = preg_replace(
                ArticleParsingHelper::REGEX_TAG_IMG["REVERSE_OPEN"],
                ArticleParsingHelper::REGEX_TAG_IMG["REVERSE_REPLACE_OPEN"],
                $parsedContent
            );

            return $parsedContent;
        }
    }
}