<?php
namespace AModestPlatform\Plugins\Helpers;

/* -- DEPENDENCIES -- */
// Phalcon
use Phalcon\Mvc\User\Component;
use Phalcon\Security\Random;

// PHPDoc
use AModestPlatform\_configPrototype;

/* -- CLASS LayoutHelper -- */
/**
 * @property _configPrototype $config
 */
class PageLayoutHelper extends Component
{
    /* -- CONSTRUCTOR -- */
    public function __construct($di)
    {
        $this->setDI($di);
    }

    /* -- INTERNAL METHODS -- */
    // Load Global Styles and JS
    public function loadGlobalStylesAndJS(&$refController)
    {
        // Config
        $sassDir = $this->config->directories->baseURI . $this->config->directories->stylesDir;

        /* -- LOAD SASS AND GOOGLE FONTS -- */
        $refController->assets->addCss('//fonts.googleapis.com/css?family=Archivo+Black|Noto+Serif', false);
        $refController->assets->addCss('//code.jquery.com/ui/1.12.1/themes/smoothness/jquery-ui.css', false);
        $refController->assets->addCss($sassDir . 'styles.css');

        /* -- LOAD EXTERNAL JS AND CSS -- */
        // jQuery
        $refController->assets->addJs('//code.jquery.com/jquery-3.2.1.min.js',
            false,
            false,
            [
                'integrity' => 'sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4=',
                'crossorigin' => 'anonymous'
            ]
        );
        // Popper
        $refController->assets->addJs('//cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js',
            false,
            false,
            [
                'integrity' => 'sha384-b/U6ypiBEHpOf/4+1nzFpr53nxSS+GLCkfwBdFNTxtclqqenISfwAzpKaMNFNmj4',
                'crossorigin' => 'anonymous'
            ]
        );
        // Bootstrap
        $refController->assets->addJs('//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js',
            false,
            false,
            [
                'integrity' => 'sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa',
                'crossorigin' => 'anonymous'
            ]
        );
        //jQuery UI
        $refController->assets->addJs('//code.jquery.com/ui/1.12.1/jquery-ui.min.js',
            false,
            false,
            [
                'integrity' => 'sha256-VazP97ZCwtekAsvgPBSUwPFKdrwD3unUfSGVYrahUqU=',
                'crossorigin' => 'anonymous'
            ]
        );
    }

    // Load Specific Internal
    public function loadSpecificInternal(&$refController, $file, $type = "CSS")
    {
        // Load CSS File
        if ($type == "CSS")
        {
            $refController->assets->addCss($file . "?" . $this->cacheBuster());
        }
        // Load JS File (assumedly)
        else if ($type == "JS")
        {
            $refController->assets->addJs($file . "?" . $this->cacheBuster());
        }
        else
        {
            return false;
        }

        return true;
    }

    // Load Specific External
    public function loadSpecificExternal(&$refController, $url, $type = "CSS", $enableAttributes = true, $options = array(
        "Local" => false,
        "Filter" => false,
        "Attributes" => array(
            "integrity" => "",
            "crossorigin" => ""
        )
    ))
    {
        // Load CSS File
        if ($type == "CSS")
        {
            if ($enableAttributes == true)
            {
                $refController->assets->addCss($url, $options["Local"], $options["Filter"], $options["Attributes"]);
            }
            else
            {
                $refController->assets->addCss($url, $options["Local"], $options["Filter"]);
            }
        }
        // Load JS File
        else if ($type == "JS")
        {
            if ($enableAttributes == true)
            {
                $refController->assets->addJs($url, $options["Local"], $options["Filter"], $options["Attributes"]);
            }
            else
            {
                $refController->assets->addJs($url, $options["Local"], $options["Filter"]);
            }
        }
        else
        {
            return false;
        }

        return true;
    }

    /* -- PROTECTED METHODS -- */
    protected function cacheBuster()
    {
        $random = new Random();
        try
        {
            return $random->hex(5);
        }
        catch (\Exception $e)
        {
            return false;
        }
    }
}