<?php
namespace AModestPlatform\Plugins\Helpers;

/* -- DEPENDENCIES -- */
// Phalcon
use Phalcon\Di;
use Phalcon\Mvc\User\Component;
use Phalcon\Mvc\View;

// PHPDoc
use AModestPlatform\Models\User;

// Internal
use AModestPlatform\Plugins\Managers\UserManager;

/* -- CLASS UserInfoHelper -- */
class UserInfoHelper extends Component
{
    /* -- PROTECTED -- */
    /** @var $userManager UserManager */
    protected $userManager;

    /** @var $viewInstance View */
    protected $viewInstance;
    protected $viewAttached = false;

    /* -- CONSTRUCTOR -- */
    /** @param $di Di Phalcon DependencyInjector instance */
    public function __construct($di)
    {
        $this->setDI($di);
        $this->userManager = $di->get('userManager');
    }

    /* -- PUBLIC FUNCTIONS -- */
    // Init Awareness
    public function initAwareness()
    {
        if (is_array($loginResult = $this->userManager->cognizant()))
        {
            if ($loginResult["Result"] === true)
            {
                $this->view->setVar('loggedIn', true);

                /**
                 * @var User $fetchedUser
                 */
                $fetchedUser = $loginResult["User"];

                // Get our contributor status
                $isContributor = $fetchedUser->getContributorId();
                if ($isContributor == 0)
                {
                    $this->view->setVar('isContributor', false);
                }
                else
                {
                    $this->view->setVar('isContributor', true);
                    $this->view->setVar('contributorID', $isContributor);
                }

                return $fetchedUser;
            }

            return false;
        }

        return false;
    }
}