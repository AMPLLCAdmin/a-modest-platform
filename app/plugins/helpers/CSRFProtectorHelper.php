<?php
// NOTICE: The burden for generating CSRF keys via Phalcon is too high and decreases performance of the website
// significantly, so this was written instead. Maybe can switch to Phalcon's CSRF protection method in the future
// if performance for the generation of CSRF tokens is improved
namespace AModestPlatform\Plugins\Helpers;

/* -- DEPENDENCIES -- */
// PHP
use \DateTime;

// Phalcon
use Phalcon\Di;
use Phalcon\Mvc\User\Component;
use Phalcon\Session\Adapter\Files as SessionAdapter;

/* -- CLASS CSRFProtector -- */
class CSRFProtectorHelper extends Component
{
    /* -- PROTECTED -- */
    /** @var SessionAdapter $session */
    protected $session;

    /* -- CONSTRUCTOR -- */
    /**
     * CSRFProtectorHelper constructor.
     * @param $di Di Phalcon DependencyInjector instance
     */
    public function __construct($di)
    {
        $this->setDI($di);
        $this->session = $di->get('session');
    }

    /* -- PUBLIC CLASS METHODS -- */
    // CSRF - Generates or persists a CSRF instance and returns the token
    public function CSRF()
    {
        if ($this->checkCSRF() === false)
        {
            $this->generateCSRF();
            $CSRF = $this->session->get("AMP_CSRF");
            return $CSRF['token'];
        }
        else
        {
            $CSRF = $this->session->get("AMP_CSRF");
            return $CSRF['token'];
        }
    }

    // Validates two CSRF tokens against each other
    public function validateCSRF($submittedCSRF)
    {
        if ($submittedCSRF == $this->CSRF())
        {
            return true;
        }

        //Invalid
        return false;
    }

    /* -- PROTECTED CLASS METHODS -- */
    // Generate CSRFKey
    protected function generateCSRF()
    {
        $now = new DateTime();
        $userAgent = $this->request->getUserAgent();

        $this->session->set("AMP_CSRF", [
            'token' => md5(CSRF_KEY . $now->getTimestamp() . $userAgent),
            'generatedTime' => new DateTime()
        ]);
    }

    // Check CSRF for validity (CSRF's only last for the current day)
    protected function checkCSRF()
    {
        if ($this->session->has('AMP_CSRF'))
        {
            $CSRF = $this->session->get('AMP_CSRF');

            $now = new DateTime();

            if ($now->diff($CSRF['generatedTime'])->days >= 1)
            {
                $this->session->destroy("AMP_CSRF");
                return false;
            }
            else
            {
                return true;
            }
        }
        else
        {
            return false;
        }
    }
}