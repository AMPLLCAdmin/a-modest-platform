<?php
namespace AModestPlatform;

require_once("../app/plugins/helpers/ArticleParsingHelper.php");

use AModestPlatform\Plugins\Helpers\ArticleParsingHelper;

$articleParser = new ArticleParsingHelper(null);

$htmlText = <<<HTML
<p><strong>Lorem</strong> ipsum dolor sit amet, consectetur adipiscing <strong>elit</strong>. Curabitur finibus luctus justo, eu scelerisque nisl aliquet quis. Nunc eget efficitur nulla. Mauris eget fermentum metus. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Phasellus vulputate magna eget finibus tincidunt. Aliquam fermentum porta lorem ac interdum. Praesent sit amet malesuada justo. Nam in faucibus enim. Aenean laoreet sit amet odio a egestas. Donec rutrum purus vitae felis lacinia mattis. Proin at augue at mi vehicula tincidunt. </p>
<p><br></p>
<p>Ut fringilla at ligula sed porttitor. Morbi quis lacus et arcu bibendum feugiat. Suspendisse ipsum elit, vulputate ut arcu eget, pharetra volutpat turpis. Cras aliquet mattis lorem, ac condimentum ex suscipit et. Donec rutrum mauris ac urna varius dapibus. Vivamus viverra mollis nibh in faucibus. Morbi luctus risus et nisl luctus eleifend eget et libero. Morbi consectetur, orci a aliquam eleifend, metus tellus consectetur urna, at ultricies augue felis ac odio.Donec in massa semper, sagittis leo vitae, malesuada neque. Nunc finibus eleifend purus, vitae tincidunt nunc finibus vitae. Etiam semper velit non nibh aliquam sodales ut eu odio. Pellentesque imperdiet convallis facilisis. Pellentesque ut erat nulla. Aenean sollicitudin nisl in metus mattis malesuada. Donec iaculis id purus eget hendrerit. Proin nec iaculis mauris. Integer nec sapien tortor. </p>
<p><br></p>
<p>Pellentesque a tortor rhoncus felis ultricies <strong>sollicitudin</strong>. Morbi quis varius mauris. Maecenas at tincidunt libero, ac dictum nibh.In tempor, lacus eu ullamcorper scelerisque, neque neque viverra mauris, sed consectetur dolor mi id libero. Nam blandit semper placerat. Vestibulum tellus justo, efficitur id felis quis, sollicitudin egestas metus. Aenean enim nibh, porttitor at euismod sed, lacinia a risus. Proin ultricies nunc blandit hendrerit blandit. Vestibulum fringilla nulla dolor, sed malesuada urna consectetur sit amet. Ut ultrices facilisis mi, et aliquet augue. Etiam sollicitudin tincidunt turpis vitae condimentum. Curabitur quis vulputate orci. Cras vel urna ac sem aliquam feugiat sed ut mi. </p>
<p><br></p>
<p>Maecenas lectus mauris, venenatis auctor facilisis vitae, ultrices id velit. Morbi rutrum egestas ligula. Aliquam aliquam vehicula feugiat.Integer posuere est vel quam iaculis fermentum. Nam rhoncus ligula at sem egestas, fermentum consectetur justo sollicitudin. Donec rhoncus felis nulla, sed tempor magna lobortis non. Nulla suscipit commodo turpis, quis viverra nulla semper vel. Suspendisse potenti. Suspendisse tincidunt porttitor faucibus. Vestibulum ut sapien vel nibh porta dignissim sit amet sit amet mauris. </p>
<p><br></p>
<p>Proin ullamcorper lectus ac mauris accumsan, vitae hendrerit nulla sollicitudin. Pellentesque ac ex sit amet risus laoreet dictum. Maecenas pretium lectus at massa volutpat, et fermentum felis malesuada. Ut eu augue et dui dapibus semper. Interdum et malesuada fames ac ante ipsum primis in faucibus.</p>
HTML;

$parsedToProper = $articleParser->parse($htmlText);
$parsedToProperReversed = $articleParser->parse($parsedToProper, true);

echo "<strong>Parsed to proper tags:</strong><br /><br />";
echo $parsedToProper;
echo "<br /><br /><hr /><br />";
echo "<strong>Parsed proper back to HTML tags:</strong><br /><br />";
echo $parsedToProperReversed;