<?php
namespace AMPImageServer\Lib;

/* -- DEPENDENCIES -- */
use \ZipArchive;

/* -- CLASS FileHandler -- */
class FileHandler
{
    /* -- CONSTANTS -- */
    public const IMGARCHIVE_FETCHBY_INDEX = 0;
    public const IMGARCHIVE_FETCHBY_NAME = 1;

    /* -- PUBLIC CLASS METHODS -- */
    // Cache - Create Image Archive
    /**
     * @param DBHandler $db
     * @param string $refId
     * @return array|bool
     */
    public function cache_createImgArchive($db, $refId)
    {
        // Sanitize RefId
        $sanitizedRefId = addslashes(htmlentities(strip_tags(str_replace('/', '', $refId))));

        // Fetch Archive Blob
        if (!($archiveBlob = $db->fetchArchiveBlob($refId)))
        {
            return false;
        }
        else
        {
            $cachedArchiveFile = CACHE_PATH . "/{$sanitizedRefId}-cache.zip";

            // Create and return the archive file
            return [$cachedArchiveFile, file_put_contents($cachedArchiveFile, $archiveBlob)];
        }
    }

    // Cache - Fetch Image From Archive
    public function cache_fetchImgFromArchive($cachedArchiveFile, $flags = 0, $imgIndex = 0, $imgName = "")
    {
        // Open image archive zip
        $cachedArchive = new ZipArchive();
        if ($cachedArchive->open($cachedArchiveFile, ZipArchive::CHECKCONS) !== true)
        {
            return false;
        }

        // Get Image Content
        if ($flags == FileHandler::IMGARCHIVE_FETCHBY_INDEX)
        {
            $img = $cachedArchive->getFromIndex($imgIndex);
        }
        else if ($flags == FileHandler::IMGARCHIVE_FETCHBY_NAME)
        {
            $img = $cachedArchive->getFromName($imgName);
        }
        else
        {
            return false;
        }

        // Close Archive
        $cachedArchive->close();

        // Return
        return [$cachedArchiveFile, $img];
    }

    // Cache - Fetch Archive Image List
    public function cache_fetchImgListFromArchive($cachedArchiveFile)
    {
        // Open image archive zip
        $cachedArchive = new ZipArchive();
        if ($cachedArchive->open($cachedArchiveFile, ZipArchive::CHECKCONS) !== true)
        {
            return false;
        }

        // Get list
        $fileNames = [];
        for ($i = 0; $i < $cachedArchive->numFiles; $i++)
        {
            array_push($fileNames, $cachedArchive->getNameIndex($i));
        }

        // Close archive
        $cachedArchive->close();

        return $fileNames;
    }

    // Cache - Insert Image Into Archive
    public function cache_insertImgIntoArchive($cachedArchiveFile, $cachedFileName, $targetName)
    {
        // Open image archive zip
        $cachedArchive = new ZipArchive();
        if ($cachedArchive->open($cachedArchiveFile, ZipArchive::CHECKCONS) !== true)
        {
            return false;
        }

        // Insert file
        $insertedFile = $cachedArchive->addFile($cachedFileName, $targetName);
	    $cachedArchive->close();

        return $insertedFile;
    }

    // Cache - Get Archive Blob
	public function cache_getArchiveBlob($cachedArchiveFile)
	{
		$fo = fopen($cachedArchiveFile, "rb");
		$fdata = fread($fo, filesize($cachedArchiveFile));
		fclose($fo);

		return addslashes($fdata);
	}

    // Cache - Destroy File
    public function cache_destroyFile($cachedFile)
    {
        return unlink($cachedFile);
    }

    // Upload - Move to Cache
    public function upload_moveToCache($refId, $temp, $permanent)
    {
        // Sanitize RefId
        $sanitizedRefId = addslashes(htmlentities(strip_tags(str_replace('/', '', $refId))));

        $fileDestination = CACHE_PATH . "/" . $sanitizedRefId . "-temp-" . $permanent;

        return @[$fileDestination, move_uploaded_file($temp, $fileDestination)];
    }
}