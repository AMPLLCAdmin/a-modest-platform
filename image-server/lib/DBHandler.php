<?php
namespace AMPImageServer\Lib;

// Dependencies
use \mysqli;

/* -- CLASS FileHandler -- */
class DBHandler
{
    /* -- PRIVATE DECLARATIONS -- */
    /** @var $db mysqli */
    protected $db = null;

    /* -- PUBLIC METHODS-- */
    // Connect
    public function connect($db_server = null, $db_username = null, $db_password = null, $db_name = null)
    {
        if ($db_server == null)
        {
            @$conn = new mysqli(DB_SERVER, DB_USERNAME, DB_PASSWORD, DB_NAME);
            if ($conn->connect_error)
            {
                return $conn->connect_error;
            }
            else
            {
                $this->db = $conn;
                return true;
            }
        }
        else
        {
            $conn = new mysqli($db_server, $db_username, $db_password, $db_name);
            if ($conn->connect_error)
            {
                return $conn->connect_error;
            }
            else
            {
                $this->db = $conn;
                return true;
            }
        }
    }

    // Disconnect
    public function disconnect()
    {
        if (!is_null($this->db))
        {
            $this->db->close();
            return true;
        }
        else
        {
            return false;
        }
    }

    // Fetch Highlight Image
    public function fetchHighlightImg($refId)
    {
        if (!is_null($this->db))
        {
            $sql = "SELECT highlight_img FROM amp_article_images WHERE ref_id = '$refId'";
            $result = $this->db->query($sql);

            if ($result->num_rows > 0)
            {
                $row = $result->fetch_assoc();
                return $row['highlight_img'];
            }
            else
            {
                return false;
            }
        }
        else
        {
            return false;
        }
    }

    // Fetch Archive Blob
    public function fetchArchiveBlob($refId)
    {
        if (!is_null($this->db))
        {
            $sql = "SELECT img_archive FROM amp_article_images WHERE ref_id='$refId'";
            $result = $this->db->query($sql);

            if ($result->num_rows > 0)
            {
                $row = $result->fetch_assoc();
                return $row['img_archive'];
            }
            else
            {
                return false;
            }
        }
        else
        {
            return false;
        }
    }

    // Update Archive Blob
    public function updateArchiveBlob($refId, $newArchiveBlob)
    {
        if (!is_null($this->db))
        {
            $sql = "UPDATE amp_article_images SET img_archive='$newArchiveBlob' WHERE ref_id='$refId'";
            $this->db->query($sql);

            if ($this->db->affected_rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        else
        {
            return false;
        }
    }
}