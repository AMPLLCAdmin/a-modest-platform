<?php
/* -- NAMESPACE -- */
namespace AMPImageServer;

/* -- HANDLE CORS -- */
// Allow from any origin
if (isset($_SERVER['HTTP_ORIGIN'])) {
	header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
	header('Access-Control-Allow-Credentials: true');
	header('Access-Control-Max-Age: 86400');    // cache for 1 day
}

// Access-Control headers are received during OPTIONS requests
if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {

	if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
		header("Access-Control-Allow-Methods: GET, POST, OPTIONS");

	if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
		header("Access-Control-Allow-Headers:        {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");
}

/* -- IMPORT -- */
require_once('config/config.php');
require_once('lib/FileHandler.php');
require_once('lib/DBHandler.php');

/* -- DEPENDENCIES -- */
use AMPImageServer\Lib\FileHandler;
use AMPImageServer\Lib\DBHandler;

// BUFFER OUTPUT
ob_start();

/* -- CONNECT TO THE DATABASE -- */
$DB = new DBHandler();
if (!(($connection = $DB->connect()) === true))
{
    die("Fatal Database Connection Error: $connection");
}

/* -- PROCESS GET PARAMETERS -- */
$refId = isset($_GET['refid']) ? $_GET['refid'] : die("Fatal Error: Reference ID is a required parameter!");
$type = isset($_GET['type']) ? $_GET['type'] : die("Fatal Error: Fetch type is a required parameter!");

/* -- GET ARCHIVE FILE LIST -- */
if ($type == "archiveList")
{
    // Sanitize RefId
    $sanitizedRefId = addslashes(htmlentities(strip_tags(str_replace('/', '', $refId))));

    $fhandle = new FileHandler();

    $cachedArchive = $fhandle->cache_createImgArchive($DB, $sanitizedRefId);

    if (is_array($cachedArchive))
    {
        $fileList = $fhandle->cache_fetchImgListFromArchive($cachedArchive[0]);
        echo json_encode($fileList);
        $fhandle->cache_destroyFile($cachedArchive[0]);
    }
    else
    {
        echo 0;
    }
}

// FLUSH OUTPUT
ob_flush();

/* -- CLOSE DATABASE CONNECTION -- */
$DB->disconnect();

