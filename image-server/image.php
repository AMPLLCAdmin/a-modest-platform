<?php
/**
 * IMAGE SERVER
 *
 * Delivers dynamic images from a database to a Phalcon-enabled application
 * Version: 1.0.0
 * First Released: October 19th, 2017 (Version 0.0.1 - Beta)
 * Last Update: October 26th, 2017 (Version 1.0.0 - Stable)
 *
 * Designed by Zach Meyer
 * Originally designed for A Modest Platform LLC.
 */

/* -- NAMESPACE -- */
namespace AMPImageServer;

/* -- HANDLE CORS -- */
// Allow from any origin
if (isset($_SERVER['HTTP_ORIGIN'])) {
	header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
	header('Access-Control-Allow-Credentials: true');
	header('Access-Control-Max-Age: 86400');    // cache for 1 day
}

// Access-Control headers are received during OPTIONS requests
if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {

	if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
		header("Access-Control-Allow-Methods: GET, POST, OPTIONS");

	if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
		header("Access-Control-Allow-Headers:        {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");
}

/* -- IMPORT -- */
require_once('config/config.php');
require_once('lib/FileHandler.php');
require_once('lib/DBHandler.php');

/* -- DEPENDENCIES -- */
use AMPImageServer\Lib\FileHandler;
use AMPImageServer\Lib\DBHandler;

// BUFFER OUTPUT
ob_start();

/* -- CONNECT TO THE DATABASE -- */
$DB = new DBHandler();
if (!(($connection = $DB->connect()) === true))
{
    die("Fatal Database Connection Error: $connection");
}

/* -- PROCESS GET PARAMETERS -- */
$refId = isset($_GET['refid']) ? $_GET['refid'] : die("Fatal Error: Reference ID is a required parameter!");
$type = isset($_GET['type']) ? $_GET['type'] : die("Fatal Error: Request type is a required parameter!");

/* -- GET HIGHLIGHT IMAGE -- */
if ($type == 'highlight')
{
    // Sanitize RefId
    $sanitizedRefId = addslashes(htmlentities(strip_tags(str_replace('/', '', $refId))));

    // Get the highlight image
    if (!($image = $DB->fetchHighlightImg($sanitizedRefId)))
    {
        die("Fatal Error: Highlight Image for Reference ID $sanitizedRefId could not be located!");
    }

    // Set headers and send image
    header("HTTP/1.1 200 OK");
    header("Content-Type: image/png");
    header("Cache-Control: public, max-age=86400");

    echo $image;
}

/* -- GET IMAGE FROM ARCHIVE -- */
if ($type == 'archive')
{
    $index = isset($_GET['index']) ? $_GET['index'] : die("Fatal Error: Image index is a required parameter when fetching by archive!");
    $fhandle = new FileHandler();

    $cachedArchive = $fhandle->cache_createImgArchive($DB, $refId);
    $imgdata = $fhandle->cache_fetchImgFromArchive($cachedArchive[0], FileHandler::IMGARCHIVE_FETCHBY_INDEX, $index);

    // Set headers and send image
    header("HTTP/1.1 200 OK");
    header("Content-Type: image/png");
    header("Cache-Control: public, max-age=86400");

    echo $imgdata[1];

    // Destroy the cached archive
    $fhandle->cache_destroyFile($imgdata[0]);
}

/* -- UPLOAD IMAGE INTO ARCHIVE -- */
if ($type == 'upload')
{
    $tempFile = $_FILES['fileData']['tmp_name'];
    $fileName = $_FILES['fileData']['name'];

    $fhandle = new FileHandler();

    // Move uploaded image to cache directory
    $cacheFile = $fhandle->upload_moveToCache($refId, $tempFile, $fileName);

    // Create a new archive file from the database and insert cached image file into archive
	$cachedArchive = $fhandle->cache_createImgArchive($DB, $refId);

	// Check if archive was retrieved successfully
	if (!is_bool($cachedArchive))
	{
		$isMoved = $fhandle->cache_insertImgIntoArchive($cachedArchive[0], $cacheFile[0], $fileName);

		// Check if file was added to archive successfully
		if ($isMoved)
		{
			$newArchiveBlob = $fhandle->cache_getArchiveBlob($cachedArchive[0]);

			$updatedBlob = $DB->updateArchiveBlob($refId, $newArchiveBlob);

			// Destroy cached file
			$fhandle->cache_destroyFile($cacheFile[0]);
			$fhandle->cache_destroyFile($cachedArchive[0]);

			if ($updatedBlob)
			{
				echo "Upload successful!";
			}
			else
			{
				echo "Upload failed - Error Code 003!";
			}
		}
		else
		{
			echo "Upload failed - Error code 002!";
		}
	}
	else
	{
		echo "Upload failed - Error code 001!";
	}
}

// FLUSH OUTPUT
ob_flush();

/* -- CLOSE DATABASE CONNECTION -- */
$DB->disconnect();





